import { useTranslation } from 'react-i18next';

export const useTrans = () => {
  const { t } = useTranslation();
  return t;
};
