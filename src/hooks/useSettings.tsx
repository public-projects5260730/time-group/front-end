import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { from } from "rxjs";
import { settingApi } from "../apis";
import { setSettings } from "../store/actions";

const useSettings = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const subscription = from(settingApi.findAll()).subscribe(
      ({ settings }) => {
        dispatch(setSettings(settings || {}));
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, [dispatch]);
};

export default useSettings;
