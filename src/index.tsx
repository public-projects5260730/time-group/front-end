import * as ReactDOM from "react-dom/client";

import { Provider } from "react-redux";
import { store, persistor } from "./store";

import { PersistGate } from "redux-persist/integration/react";
import { I18nextProvider } from "react-i18next";
import { languageConfiguration } from "./languages";
import Routes from "./routes";

import "./resources/app.scss";

const root = ReactDOM.createRoot(document.getElementById(
  "root"
) as HTMLElement);

root.render(
  <Provider store={store}>
    <I18nextProvider i18n={languageConfiguration}>
      <PersistGate loading={null} persistor={persistor}>
        <Routes />
      </PersistGate>
    </I18nextProvider>
  </Provider>
);
