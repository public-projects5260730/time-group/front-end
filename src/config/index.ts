let config: {
  apiUrl: string;
  storeLifeTime: number;
};

if (window?._env_?.REACT_APP_API) {
  config = {
    apiUrl: window?._env_?.REACT_APP_API || "",
    storeLifeTime: +window?._env_?.REACT_APP_STORE_LIFE_TIME || 0,
  };
}

if (process.env.NODE_ENV !== "production") {
  config = {
    apiUrl: process.env?.["REACT_APP_API"] || "",
    storeLifeTime: +(process.env?.["REACT_APP_STORE_LIFE_TIME"] || 0),
  };
}
export { config };
