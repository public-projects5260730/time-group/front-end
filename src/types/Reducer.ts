import { Action } from "redux";
import { ThunkAction } from "redux-thunk";
import { rootReducer } from "../store/rootReducer";

export type AppState = ReturnType<typeof rootReducer>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  any, // or some ThunkExtraArgument interface
  Action<string>
>;

// the tricky part: convert each `(args) => AppThunk => void` to `(args) => void`
export type ThunkProps<
  T extends { [K in keyof T]: (...a: any[]) => AppThunk<void> }
> = { [K in keyof T]: (...args: Parameters<T[K]>) => void };
