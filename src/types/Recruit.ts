export interface IRecruit {
  id: number;
  title: string;
  body: string;
  work_type: string;
  location: string;
  pay_rate: string;
  position: string;
  specialisms: string;
  expired_at: number;
  created_at: number;
  updated_at: number;
}

export interface IRecruitPaginateParams {
  search?: string;
  specialisms?: string;
  location?: string;
  page?: number;
  perPage?: number;
}

export interface ISendCVInputs {
  email?: string;
  phone?: string;
  fullName?: string;
  cvFile?: FileList;
}
