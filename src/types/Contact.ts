export interface ISendMessageInputs {
  email: string;
  message: string;
}
