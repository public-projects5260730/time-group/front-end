// export interface IApiResponse<T = unknown> {
//   data?: T;
//   error?: string;
//   errors?: Record<string, string>;
//   success?: boolean;
// }

export type IApiResponse<T = unknown> = T & {
  data?: T;
  error?: string;
  errors?: Record<string, string>;
  success?: boolean;
  successfully?: boolean;
};

export type IApiFuntion<T, K = unknown> = (
  params: T
) => Promise<IApiResponse<K>>;

export type IPaginate<T> = T & {
  current_page: number;
  data: T[];
  from: number;
  last_page: number;
  per_page: number;
  to: number;
  total: number;
};
