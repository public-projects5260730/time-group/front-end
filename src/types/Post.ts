export interface Posts {
  id: number;
  author_id: number;
  title: string;
  seo_title: null;
  excerpt: string;
  body: string;
  image: string;
  slug: string;
  meta_description: string;
  meta_keywords: string;
  status: string;
  featured: number;
  created_at: number;
  updated_at: number;
}

export interface IPost extends Posts {}

export interface IPostParams {
  categories?: {
    id?: number | string;
    slug?: string;
  };
  id?: number | string;
  slug?: string;
  featured?: 0 | 1;
  limit: number;
  page?: number;
  perPage?: number;
  search?: string;
}

export interface IPostPaginateParams extends Omit<IPostParams, "limit"> {}
