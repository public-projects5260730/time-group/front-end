import { IBaseStoreState } from "./Store";

export interface IAuthState extends IBaseStoreState {
  accessToken: string;
}

export interface ILoginData {
  email: string;
  password: string;
  rememberMe: boolean;
}

export interface IRegisterData {
  fullName?: string;
  email: string;
  password: string;
  passwordConfirm: string;
  referralCode?: string;
  phone?: boolean;
}

export interface IForgotPasswordData {
  email: string;
}

export interface IForgotPasswordConfirmData {
  c: string;
  m: string;
  password: string;
  passwordConfirm: string;
}

export interface IAccountConfirmData {
  c: string;
  m: string;
}
