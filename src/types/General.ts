export type ActionType<T> = T extends (...arg: any) => any
  ? ReturnType<T>
  : {
      [K in keyof T]: ActionType<T[K]>;
    }[keyof T];
