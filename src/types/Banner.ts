export interface IBanner {
  id: number;
  title: string;
  image: string;
  positions: string;
  order: number;
  link: string;
}

export interface IBannerParams {
  positions?: string;
}
