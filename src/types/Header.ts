import React from 'react';

export interface IBreadcrumb {
  title: string;
  link?: string;
  active?: boolean;
  icon?: string;
}

export interface IHeaderState {
  section: string;
  pageTitle: string;
  breadcrumbs: IBreadcrumb[];
  isToggleSidebar: boolean;
  headerOpen: boolean;
  quickActions: React.ReactNode;
}
