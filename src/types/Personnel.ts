export interface IPersonnel {
  id: number;
  name: string;
  position: string;
  image: string;
  order: number;
  created_at: Date;
  updated_at: Date;
}
