export interface IBaseStoreState {
  __hash?: string;
  __lifeTime?: number;
  __updateAt?: number;
}

export interface ICacheState {
  caches: string[];
}
