export interface ICompanyGroup {
  id: number;
  name: string;
  order: number;
  created_at: number;
  updated_at: number;
  companies: ICompany[];
}

export interface ICompany {
  id: number;
  name: string;
  company_group_id: number;
  image: string;
  logo: string;
  address: string;
  email: string;
  phone: string;
  website: string;
  brief_intro: string;
  personel: string;
  position: string;
  order: number;
  created_at: number;
  updated_at: number;
}
