export {};
declare global {
  interface Window {
    _env_: {
      REACT_APP_API: string;
      REACT_APP_STORE_LIFE_TIME: number;
    };
  }
}
