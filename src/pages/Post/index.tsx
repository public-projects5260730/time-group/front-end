import { LazyLoadAble } from "../../utils";

export const PostViewPage = LazyLoadAble(() => import("./PostViewPage"));
