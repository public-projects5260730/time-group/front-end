import { useNavigate, useParams } from "react-router-dom";
import { PostViewBannerContainer } from "../../component/Posts/Banner";
import { RelatedPostsContainer } from "../../component/Posts/Related";
import { PostViewContainer } from "../../component/Posts/View";
import { Helmet } from "react-helmet";

import "./../../resources/posts/view.scss";

const PostViewPage = () => {
  const { slug } = useParams<{ slug: string }>();
  const history = useNavigate();
  if (!slug?.length) {
    history("/");
    return <></>;
  }

  return (
    <main>
      <div className="container post-view-page">
        <div className="post-view-column">
          <PostViewContainer slug={slug} />
          <PostViewBannerContainer />
        </div>
      </div>
      <RelatedPostsContainer slug={slug} />
    </main>
  );
};

export default PostViewPage;
