import { LazyLoadAble } from "../../utils";

export const IntroducePage = LazyLoadAble(() => import("./IntroducePage"));
