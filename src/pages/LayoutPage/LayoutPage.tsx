import React from "react";
import { FooterComponent, HeaderComponent } from "../../component";
import { Outlet } from "react-router-dom";
import Tools from "./tools";

const LayoutPage = () => {
  return (
    <div className="wrapper">
      <label htmlFor="menu-mobile-input" className="menu-mobile__label">
        <HeaderComponent />
        <Outlet />
        <FooterComponent />
      </label>
      <Tools />
    </div>
  );
};

export default LayoutPage;
