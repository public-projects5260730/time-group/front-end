import { LazyLoadAble } from "../../utils";

export const LayoutPage = LazyLoadAble(() => import("./LayoutPage"));
