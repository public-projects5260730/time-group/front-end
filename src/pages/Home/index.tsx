import { LazyLoadAble } from "../../utils";

export const HomePage = LazyLoadAble(() => import("./HomePage"));
