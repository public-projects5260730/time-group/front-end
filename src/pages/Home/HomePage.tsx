import React from "react";
import { HomeContainer } from "../../component";
import { siteName } from "../../utils";

const HomePage = () => {
  // window.document.title = `${siteName} - Trang chủ`;

  return <HomeContainer />;
};

export default HomePage;
