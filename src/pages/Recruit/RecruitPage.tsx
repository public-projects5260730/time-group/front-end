import { RecruitHeaderComponent } from "../../component/Recruit/Header";
import { RecruitListContainer } from "../../component/Recruit/list";
import { siteName } from "../../utils";
import "./../../resources/recruit/recruit.scss";

const RecruitPage = () => {
  // window.document.title = `${siteName} - Tuyển dụng`;

  return (
    <div className="recruit-page">
      <RecruitHeaderComponent />
      <RecruitListContainer />
    </div>
  );
};

export default RecruitPage;
