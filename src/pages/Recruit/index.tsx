import { LazyLoadAble } from "../../utils";

export const RecruitPage = LazyLoadAble(() => import("./RecruitPage"));
