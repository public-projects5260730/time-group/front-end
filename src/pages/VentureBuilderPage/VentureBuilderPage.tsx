import React from "react";
import { VentureBuilderContainer } from "../../component/VentureBuilder";
import { siteName } from "../../utils";

const VentureBuilderPage = () => {
  // window.document.title = `${siteName} - Venture Builder`;

  return <VentureBuilderContainer />;
};

export default VentureBuilderPage;
