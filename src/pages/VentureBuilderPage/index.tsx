import { LazyLoadAble } from "../../utils";

export const VentureBuilderPage = LazyLoadAble(() =>
  import("./VentureBuilderPage")
);
