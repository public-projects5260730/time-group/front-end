import { NewsSearchForm } from "../../component/News/search-form";
import { SearchNewsContainer } from "../../component/News/search-news";

const SearchNewsPage = () => {
  return (
    <main>
      <div className="container mt-4">
        <NewsSearchForm />
        <SearchNewsContainer />
      </div>
    </main>
  );
};

export default SearchNewsPage;
