import { LazyLoadAble } from "../../utils";

export const NewsPage = LazyLoadAble(() => import("./NewsPage"));
export const SearchNewsPage = LazyLoadAble(() => import("./SearchNewsPage"));
