import { FeaturedNewsContainer } from "../../component/News/featured";
import { NewsContainer } from "../../component/News/news";
import { NewsSearchForm } from "../../component/News/search-form";
import { siteName } from "../../utils";

const NewsPage = () => {
  // window.document.title = `${siteName} - Tin tức`;

  return (
    <main>
      <div className="featured-new love-seat-bg">
        <div className="container">
          <NewsSearchForm />
          <FeaturedNewsContainer />
        </div>
      </div>

      <div className="container">
        <NewsContainer />
      </div>
    </main>
  );
};

export default NewsPage;
