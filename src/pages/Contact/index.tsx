import { LazyLoadAble } from "../../utils";

export const ContactPage = LazyLoadAble(() => import("./ContactPage"));
