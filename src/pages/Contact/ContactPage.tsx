import { ContactGoogleMap } from "../../component/Contact";
import { ContactSendMessageForm } from "../../component/Contact/ContactSendMessageForm";
import { siteName } from "../../utils";

import "./../../resources/contact.scss";

const ContactPage = () => {
  // window.document.title = `${siteName} - Liên hệ`;

  return (
    <main className="contact-page love-seat-bg">
      <div className="container">
        <ContactGoogleMap />
        <ContactSendMessageForm />
      </div>
    </main>
  );
};

export default ContactPage;
