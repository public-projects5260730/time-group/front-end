import { LazyLoadAble } from "../../utils";

export const EcosystemPage = LazyLoadAble(() => import("./EcosystemPage"));
