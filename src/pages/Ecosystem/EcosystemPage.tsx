import {
  EcosystemMainContainer,
  EcosystemMainTopComponent,
} from "../../component/Ecosystem";
import { siteName } from "../../utils";

const EcosystemPage = () => {
  // window.document.title = `${siteName} - Hệ sinh thái`;

  return (
    <div className="ecosystem-page">
      <EcosystemMainTopComponent />
      <div className="love-seat-bg ecosystem-companies">
        <EcosystemMainContainer />
      </div>
    </div>
  );
};

export default EcosystemPage;
