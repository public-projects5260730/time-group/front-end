import { Route, Routes } from "react-router-dom";
import { ContactPage } from "../pages/Contact";

const ContactRoutes = () => {
  return (
    <Routes>
      <Route index element={<ContactPage />} />
    </Routes>
  );
};

export default ContactRoutes;
