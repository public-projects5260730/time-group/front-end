import { Route, Routes } from "react-router-dom";
import EcosystemPage from "../pages/Ecosystem/EcosystemPage";

const EcosystemRoutes = () => {
  return (
    <Routes>
      <Route index element={<EcosystemPage />} />
    </Routes>
  );
};

export default EcosystemRoutes;
