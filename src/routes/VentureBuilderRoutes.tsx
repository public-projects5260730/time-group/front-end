import React from "react";
import { Route, Routes } from "react-router-dom";
import { VentureBuilderPage } from "../pages";

const VentureBuilderRoutes = () => {
  return (
    <Routes>
      <Route index element={<VentureBuilderPage />} />
    </Routes>
  );
};

export default VentureBuilderRoutes;
