import { Route, Routes } from "react-router-dom";
import { NewsPage, SearchNewsPage } from "../pages/News";

const NewsRoutes = () => {
  return (
    <Routes>
      <Route index element={<NewsPage />} />
      <Route path="search" element={<SearchNewsPage />} />
    </Routes>
  );
};

export default NewsRoutes;
