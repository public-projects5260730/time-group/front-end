import React from "react";
import { Route, Routes } from "react-router-dom";
import { IntroducePage } from "../pages";

const IntroduceRoutes = () => {
  return (
    <Routes>
      <Route index element={<IntroducePage />} />
    </Routes>
  );
};

export default IntroduceRoutes;
