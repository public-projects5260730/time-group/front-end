import React from "react";
import { BrowserRouter, Route, Routes as Switch } from "react-router-dom";
import { SEOComponent } from "../component/SEO";
import useSettings from "../hooks/useSettings";
import { HomePage } from "../pages";
import { LayoutPage } from "../pages/LayoutPage";
import ContactRoutes from "./ContactRoutes";
import EcosystemRoutes from "./EcosystemRoutes";
import IntroduceRoutes from "./IntroduceRoutes";
import NewsRoutes from "./NewsRoutes";
import PostsRoutes from "./PostsRoutes";
import RecruiteRoutes from "./RecruitRoutes";
import VentureBuilderRoutes from "./VentureBuilderRoutes";

const Routes = () => {
  useSettings();

  return (
    <BrowserRouter>
      <SEOComponent />
      <Switch>
        <Route path="/" element={<LayoutPage />}>
          <Route path="introduce/*" element={<IntroduceRoutes />} />
          <Route path="venture-builder/*" element={<VentureBuilderRoutes />} />
          <Route path="ecosystem/*" element={<EcosystemRoutes />} />
          <Route path="news/*" element={<NewsRoutes />} />
          <Route path="posts/*" element={<PostsRoutes />} />
          <Route path="recruit/*" element={<RecruiteRoutes />} />
          <Route path="contact-us/*" element={<ContactRoutes />} />
          <Route path="empty" element={<span></span>} />
          <Route index element={<HomePage />} />
          <Route path="*" element={<HomePage />} />
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
