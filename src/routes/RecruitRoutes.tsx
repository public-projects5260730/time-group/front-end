import { Route, Routes } from "react-router-dom";
import { RecruitPage } from "../pages/Recruit";

const RecruiteRoutes = () => {
  return (
    <Routes>
      <Route index element={<RecruitPage />} />
    </Routes>
  );
};

export default RecruiteRoutes;
