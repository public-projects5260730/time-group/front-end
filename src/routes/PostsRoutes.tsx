import { Route, Routes } from "react-router-dom";
import { PostViewPage } from "../pages/Post";

const PostsRoutes = () => {
  return (
    <Routes>
      <Route path=":slug/view" element={<PostViewPage />} />
    </Routes>
  );
};

export default PostsRoutes;
