import { setSettings } from "./actions";
import { settingApi } from "../../apis";
import { AppThunk } from "../../types";

export const fetchSettings = (): AppThunk => async (dispatch) => {
  await settingApi.findAll().then(({ settings }) => {
    dispatch(setSettings(settings || {}));
  });
};
