import { Action } from "redux";
import { ISettingState } from "../../types";
import { SET_SETTINGS } from "./types";

export const setSettings = (
  settings: Record<string, any>
): Action<typeof SET_SETTINGS> & Pick<ISettingState, "settings"> => {
  return { type: SET_SETTINGS, settings };
};
