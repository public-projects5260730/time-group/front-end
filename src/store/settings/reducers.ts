import { Reducer } from "redux";
import { SET_SETTINGS } from "./types";
import * as actions from "./actions";
import { ActionType, ISettingState } from "../../types";

type AdmincpActions = ActionType<typeof actions>;

const initState: ISettingState = {
  settings: {},
};

const settingsReducer: Reducer<ISettingState, AdmincpActions> = (
  state = initState,
  action
) => {
  switch (action.type) {
    case SET_SETTINGS:
      return { ...state, settings: action.settings };
    default:
      return state;
  }
};

export default settingsReducer;
