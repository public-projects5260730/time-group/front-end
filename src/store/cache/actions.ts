import { Action } from 'redux';
import { ADD_CACHE, REMOVE_CACHE } from './types';

export const addCache = (
  cache: string,
): Action<typeof ADD_CACHE> & {
  cache: string;
} => {
  return { type: ADD_CACHE, cache };
};

export const removeCache = (
  cache: string,
): Action<typeof REMOVE_CACHE> & {
  cache: string;
} => {
  return { type: REMOVE_CACHE, cache };
};
