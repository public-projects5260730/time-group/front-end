import { ADD_CACHE, REMOVE_CACHE } from "./types";
import * as actions from "./actions";
import { ActionType, ICacheState } from "../../types";
type CacheActions = ActionType<typeof actions>;

const initialState: ICacheState = {
  caches: [],
};

const cacheReducer = (
  state = initialState,
  action: CacheActions & { caches: string }
) => {
  switch (action.type) {
    case ADD_CACHE:
      if (!state.caches.includes(action.cache)) {
        state.caches.push(action.cache);
        return { ...state };
      }
      return state;

    case REMOVE_CACHE:
      const index = state.caches.indexOf(action.cache);
      if (index > -1) {
        state.caches.splice(index, 1);
        return { ...state };
      }
      return state;
    default:
      return state;
  }
};
export default cacheReducer;
