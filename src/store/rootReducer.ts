import storage from "redux-persist/lib/storage";
import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";

import { headerReducer } from "./header";
import { languageReducer } from "./language";
import { cacheReducer } from "./cache";
import { authReducer } from "./auth";
import { settingsReducer } from "./settings";

const headerPersistConfig = {
  key: "header",
  storage: storage,
  blacklist: ["quickActions"],
};

export const rootReducer = combineReducers({
  header: persistReducer(headerPersistConfig, headerReducer),
  auth: authReducer,
  language: languageReducer,
  cache: cacheReducer,
  settings: settingsReducer,
});
