import { Reducer } from "redux";
import { CLEAR_AUTH } from "./types";
import * as actions from "./actions";
import { ActionType, IAuthState } from "../../types";

type AdmincpActions = ActionType<typeof actions>;

const initState: IAuthState = {
  accessToken: "",
  // __lifeTime: 300,
};
const authReducer: Reducer<IAuthState, AdmincpActions> = (
  state = initState,
  action
) => {
  switch (action.type) {
    // case SET_VISITOR:
    //   return hashStore({
    //     ...state,
    //     visitor: action.visitor,
    //   });
    // case SET_TOKENS:
    //   return {
    //     ...state,
    //     accessToken: action.accessToken,
    //   };
    case CLEAR_AUTH:
      return {
        ...initState,
      };
    default:
      return state;
  }
};

export default authReducer;
