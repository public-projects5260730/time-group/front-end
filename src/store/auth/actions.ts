import { Action } from "redux";
import { CLEAR_AUTH } from "./types";

// export const setVisitor = (
//   visitor: IVisitor,
// ): Action<typeof SET_VISITOR> & Pick<IAuthState, 'visitor'> => {
//   return { type: SET_VISITOR, visitor };
// };

// export const setTokens = (
//   accessToken: string
// ): Action<typeof SET_TOKENS> & Pick<IAuthState, "accessToken"> => {
//   return {
//     type: SET_TOKENS,
//     accessToken,
//   };
// };

export const clearAuth = (): Action<typeof CLEAR_AUTH> => {
  return { type: CLEAR_AUTH };
};
