import React from "react";
import { Action } from "redux";
import { IBreadcrumb, IHeaderState } from "../../types";
import {
  CHANGE_HEADER_OPEN,
  CHANGE_NAV_OPEN,
  CHANGE_PAGE_TITLE,
  CHANGE_SECTION,
  SET_BREADCRUMB,
  SET_QUICK_ACTIONS,
} from "./types";

export const changePageTitle = (
  pageTitle: string
): Action<typeof CHANGE_PAGE_TITLE> & Pick<IHeaderState, "pageTitle"> => {
  return { type: CHANGE_PAGE_TITLE, pageTitle };
};

export const changeSection = (
  section: string
): Action<typeof CHANGE_SECTION> & Pick<IHeaderState, "section"> => {
  return { type: CHANGE_SECTION, section };
};

export const toggleSidebar = (
  isToggleSidebar: boolean
): Action<typeof CHANGE_NAV_OPEN> & Pick<IHeaderState, "isToggleSidebar"> => {
  return { type: CHANGE_NAV_OPEN, isToggleSidebar };
};

export const changeHeaderOpen = (
  headerOpen: boolean
): Action<typeof CHANGE_HEADER_OPEN> & Pick<IHeaderState, "headerOpen"> => {
  return { type: CHANGE_HEADER_OPEN, headerOpen };
};

export const changeBreadcrumbs = (
  breadcrumbs: IBreadcrumb[]
): Action<typeof SET_BREADCRUMB> & Pick<IHeaderState, "breadcrumbs"> => {
  return { type: SET_BREADCRUMB, breadcrumbs };
};

export const changeQuickActions = (
  quickActions: React.ReactNode
): Action<typeof SET_QUICK_ACTIONS> & Pick<IHeaderState, "quickActions"> => {
  return { type: SET_QUICK_ACTIONS, quickActions };
};
