import { ActionType, IHeaderState } from "../../types";
import {
  CHANGE_HEADER_OPEN,
  CHANGE_NAV_OPEN,
  CHANGE_PAGE_TITLE,
  CHANGE_SECTION,
  SET_BREADCRUMB,
  SET_QUICK_ACTIONS,
} from "./types";

import * as actions from "./actions";
import { siteName } from "../../utils";

type HeaderActions = ActionType<typeof actions>;

const initialState: IHeaderState = {
  section: "home",
  pageTitle: siteName,
  breadcrumbs: [],
  isToggleSidebar: false,
  headerOpen: false,
  quickActions: null,
};

const headerReducer = (state = initialState, action: HeaderActions) => {
  switch (action.type) {
    case CHANGE_PAGE_TITLE:
      return { ...state, pageTitle: action.pageTitle };
    case CHANGE_SECTION:
      return { ...state, section: action.section };
    case CHANGE_NAV_OPEN:
      return { ...state, isToggleSidebar: action.isToggleSidebar };
    case CHANGE_HEADER_OPEN:
      return { ...state, headerOpen: action.headerOpen };
    case SET_BREADCRUMB:
      return { ...state, breadcrumbs: action.breadcrumbs };
    case SET_QUICK_ACTIONS:
      return { ...state, quickActions: action.quickActions };
    default:
      return state;
  }
};
export default headerReducer;
