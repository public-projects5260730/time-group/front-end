export const CHANGE_PAGE_TITLE = 'header/CHANGE_PAGE_TITLE';
export const CHANGE_SECTION = 'header/CHANGE_SECTION';
export const CHANGE_NAV_OPEN = 'header/CHANGE_NAV_OPEN';
export const CHANGE_HEADER_OPEN = 'header/CHANGE_HEADER_OPENN';
export const SET_BREADCRUMB = 'header/SET_BREADCRUMB';
export const SET_QUICK_ACTIONS = 'header/SET_QUICK_ACTIONS';
