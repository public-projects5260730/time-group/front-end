import { Store, createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import { rootReducer } from "./rootReducer";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import { AppState } from "../types";

const persistConfig = {
  key: "root",
  storage,
  blacklist: ["header", "cache"],
};

const initialState = {};
const persistedReducer = persistReducer(persistConfig, rootReducer);

const store: Store<AppState> = createStore(
  persistedReducer,
  initialState,
  applyMiddleware(ReduxThunk)
);
const persistor = persistStore(store);

export { store, persistor };
