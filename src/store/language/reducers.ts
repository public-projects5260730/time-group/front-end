import { Reducer } from "redux";
import { CHANGE_LANGUAGE } from "./types";
import * as actions from "./actions";
import { ActionType, ILanguageState } from "../../types";

type AdmincpActions = ActionType<typeof actions>;

const initState: ILanguageState = {
  code: "en",
};

const languageReducer: Reducer<ILanguageState, AdmincpActions> = (
  state = initState,
  action
) => {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      return { ...state, code: action.code };
    default:
      return state;
  }
};

export default languageReducer;
