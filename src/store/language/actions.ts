import { Action } from "redux";
import { ILanguageState } from "../../types";
import { CHANGE_LANGUAGE } from "./types";

export const setConfigs = (
  code: string
): Action<typeof CHANGE_LANGUAGE> & Pick<ILanguageState, "code"> => {
  return { type: CHANGE_LANGUAGE, code };
};
