import axios, { AxiosRequestConfig, AxiosInstance } from "axios";

import queryString from "query-string";
import { IApiResponse } from "../types";
import { store } from "../store";
import { clearAuth } from "../store/actions";
import { config } from "../config";

interface IRequestParams {
  path?: string;
  params?: Record<string, any>;
  headers?: Record<string, any>;
  config?: Partial<AxiosRequestConfig>;
}

export default class HttpClient {
  apiUrl: string;
  axiosClient: AxiosInstance;

  constructor() {
    this.apiUrl =
      (config?.apiUrl || window.location.origin).replace(/[\/\\\]]*$/, "") +
      "/api";

    this.axiosClient = axios.create({
      baseURL: this.apiUrl,
      headers: {
        "Content-Type": "application/json",
      },
      paramsSerializer: (params) => queryString.stringify(params),
    });

    // this.axiosClient.interceptors.request.use(async (config) => {
    //   const accessToken = store.getState().auth.accessToken;
    //   if (accessToken) {
    //     config.headers.Authorization = `Bearer ${accessToken}`;
    //   }
    //   config.headers.Language = store.getState().language.code || 'en';
    //   return config;
    // });

    this.axiosClient.interceptors.response.use(
      (response) => {
        return { ...response, ...response?.data, ...response?.data?.data };
      },
      (error) => {
        if ((+error?.response?.status || 0) == 401) {
          store.dispatch(clearAuth());
        }
        return Promise.resolve({
          ...error,
          ...error?.response,
          ...error?.response?.data,
        });
      }
    );
  }

  getApiUrl() {
    return this.apiUrl;
  }

  axiosGet = <T>(...props: any): Promise<IApiResponse<T>> => {
    const fn = this.axiosClient.get;
    return fn.caller(fn, props);
  };

  axiosPost = <T>(...props: any): Promise<IApiResponse<T>> => {
    const fn = this.axiosClient.post;
    return fn.caller(fn, props);
  };

  get = <T>(
    data: IRequestParams | string,
    opts: IRequestParams = {}
  ): Promise<IApiResponse<T>> => {
    let path = "";
    let params = {};
    let headers = {};
    let config = {};
    if (typeof data !== "string") {
      path = data?.path || "";
      params = data?.params || {};
      headers = data?.headers || {};
      config = data?.config || {};
    } else {
      path = data;
      params = opts?.params || {};
      headers = opts?.headers || {};
      config = opts?.config || {};
    }
    const options: AxiosRequestConfig = {
      headers,
      params,
      ...config,
    };
    return this.axiosClient.get(path, options);
  };

  post = <T>(
    data: IRequestParams | string,
    opts: IRequestParams = {}
  ): Promise<IApiResponse<T>> => {
    let path = "";
    let params = {};
    let headers = {};
    let config = {};
    if (typeof data !== "string") {
      path = data?.path || "";
      params = data?.params || {};
      headers = data?.headers || {};
      config = data?.config || {};
    } else {
      path = data;
      params = opts?.params || {};
      headers = opts?.headers || {};
      config = opts?.config || {};
    }

    const options: AxiosRequestConfig = {
      headers,
      ...config,
    };
    return this.axiosClient.post(path, params, options);
  };
}
