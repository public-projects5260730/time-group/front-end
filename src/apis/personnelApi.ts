import { httpClient } from "../helpers";
import { IApiResponse, IPersonnel } from "../types";

const prefix = "/personnels";

export const personnelApi = {
  getPersonnels: async (
    searchQuery: string
  ): Promise<IApiResponse & { personnels?: IPersonnel[] }> => {
    return httpClient.get(prefix + searchQuery);
  },
};
