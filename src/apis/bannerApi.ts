import { httpClient } from "../helpers";
import { IApiResponse, IBanner, IBannerParams } from "../types";

const prefix = "/banners";

export const bannerApi = {
  findBanners: (
    params: IBannerParams
  ): Promise<IApiResponse<{ banners?: IBanner[] }>> => {
    return httpClient.get(prefix, { params });
  },

  getBanners: async (): Promise<IApiResponse & { banners?: IBanner[] }> => {
    return httpClient.get(prefix);
  },
};
