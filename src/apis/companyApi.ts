import { httpClient } from "../helpers";
import { ICompany, IApiResponse, ICompanyGroup } from "../types";

const prefix = "/companies";

export const companyApi = {
  getGroups: (): Promise<IApiResponse<{ companyGroups?: ICompanyGroup[] }>> => {
    return httpClient.get(prefix + "/groups");
  },

  getCompanies: async (): Promise<
    IApiResponse & { companies?: ICompany[] }
  > => {
    return httpClient.get(prefix);
  },
};
