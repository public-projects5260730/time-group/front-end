import { ISendCVInputs } from "./../types/Recruit";
import { httpClient } from "../helpers";
import {
  IApiResponse,
  IPaginate,
  IRecruit,
  IRecruitPaginateParams,
} from "../types";

const prefix = "/recruits";

export const recruitApi = {
  findPaginate: (
    params?: IRecruitPaginateParams
  ): Promise<IApiResponse<{ paginate?: IPaginate<IRecruit> }>> => {
    return httpClient.get(prefix, { params });
  },

  findOptions: (): Promise<
    IApiResponse<{ locationOptions?: string[]; specialismsOptions: string[] }>
  > => {
    const url = `${prefix}/options`;
    return httpClient.get(url);
  },

  sendCv: (
    id: number,
    params: ISendCVInputs
  ): Promise<
    IApiResponse<{ locationOptions?: string[]; specialismsOptions: string[] }>
  > => {
    const formData = new FormData();

    Object.entries(params).map(([field, value]) => {
      if (field === "cvFile") {
        formData.append(field, value[0]);
      } else {
        formData.append(field, value);
      }
      return null;
    });

    const url = `${prefix}/${id}/send-cv`;
    return httpClient.post(url, {
      params: formData,
    });
  },
};
