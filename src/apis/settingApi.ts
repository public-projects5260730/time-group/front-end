import { httpClient } from "../helpers";
import { IApiResponse } from "../types";

const prefix = "/settings";
export const settingApi = {
  findAll: (): Promise<IApiResponse<{ settings?: Record<string, any> }>> => {
    return httpClient.get(prefix);
  },
};
