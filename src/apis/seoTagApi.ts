import { httpClient } from "../helpers";
import { IApiResponse } from "../types";

const prefix = "/seotags";

export const seoTagApi = {
  fetch: (uri: string): Promise<IApiResponse<{ seoTags?: any }>> => {
    return httpClient.get(prefix, { params: { uri } });
  },
};
