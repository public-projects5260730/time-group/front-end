import { IPost, IPostPaginateParams, IPostParams } from "./../types/Post";
import { httpClient } from "../helpers";
import { IApiResponse, IPaginate, Posts } from "../types";

const prefix = "/posts";

export const postApi = {
  getPosts: async (
    searchQuery: string
  ): Promise<IApiResponse & { posts?: Posts[] }> => {
    return httpClient.post(prefix + searchQuery);
  },

  findPaginate: (
    params?: IPostPaginateParams
  ): Promise<IApiResponse<{ paginate?: IPaginate<IPost> }>> => {
    return httpClient.post(prefix, { params });
  },

  findPosts: (
    params: IPostParams
  ): Promise<IApiResponse<{ posts?: IPost[] }>> => {
    return httpClient.post(prefix, { params });
  },

  findPost: (slug: string): Promise<IApiResponse<{ post?: IPost }>> => {
    return httpClient.get(`${prefix}/${slug}/view`);
  },

  findRelatedPosts: (
    slug: string
  ): Promise<IApiResponse<{ posts?: IPost[] }>> => {
    return httpClient.post(`${prefix}/${slug}/related`);
  },
};
