import { httpClient } from "../helpers";
import { IApiResponse, ISendMessageInputs } from "../types";

const prefix = "/contacts";

export const contactApi = {
  sendMessage: (params?: ISendMessageInputs): Promise<IApiResponse> => {
    const url = `${prefix}/send-message`;
    return httpClient.post(url, { params });
  },
};
