import { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { from } from "rxjs";
import { seoTagApi } from "../../apis";
import { AppState } from "../../types";
import { isDeepEqual, raw } from "../../utils";

export const SEOComponent = () => {
  const { pathname } = useLocation();

  const settings = useSelector(
    (state: AppState) => state.settings.settings,
    isDeepEqual
  );

  const [seoTags, setSeoTags] = useState<any>();

  useEffect(() => {
    const subscription = from(seoTagApi.fetch(pathname)).subscribe(
      ({ seoTags }) => {
        setSeoTags(seoTags);
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, [pathname]);

  return (
    <Helmet>
      {!!settings?.title?.length && <title>{settings?.title}</title>}
      {!!settings?.description?.length && (
        <meta name="description" content={settings.description} />
      )}
      {raw(seoTags || "")}
    </Helmet>
  );
};
