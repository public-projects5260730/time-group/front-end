import moment from "moment";
import { IPost } from "../../../types";
import { raw } from "../../../utils";

export const PostViewComponent = ({ post }: { post: IPost }) => {
  return (
    <div className="post-view font-size-30px">
      <h1>{post.title}</h1>
      <time>
        <i className="far fa-calendar mr-2"></i>
        <span>{moment.unix(post.created_at).format("LL")}</span>
      </time>
      <div className="post-content">{raw(post.body)}</div>
    </div>
  );
};
