import { useEffect, useState } from "react";

import { from } from "rxjs";
import { postApi } from "../../../apis";
import { IPost } from "../../../types";
import { siteName } from "../../../utils";
import { CardNoData, OverlayLoading } from "../../tools";
import { PostViewComponent } from "./PostView.component";

export const PostViewContainer = ({ slug }: { slug: string }) => {
  const [post, setPost] = useState<IPost | null>();

  useEffect(() => {
    const subscription = from(postApi.findPost(slug)).subscribe(({ post }) => {
      setPost(post || null);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [slug]);

  if (post === undefined) {
    return <OverlayLoading />;
  }

  if (!post?.id) {
    return <CardNoData />;
  }

  // window.document.title = `${post.title}`;

  return <PostViewComponent post={post} />;
};
