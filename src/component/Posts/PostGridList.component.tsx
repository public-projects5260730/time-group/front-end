import { Link } from "react-router-dom";
import { IPost } from "../../types";
import { imageUrl } from "../../utils";

import "./../../resources/posts/grid.scss";

export const PostGridListComponent = ({ posts }: { posts: IPost[] }) => {
  return (
    <div className="post-grid-list">
      {posts.map((post) => (
        <article className="post-grid-item post-item" key={post.id}>
          <figure className="snap post-thumb">
            <img
              className="img-fluid"
              src={imageUrl(post.image)}
              alt={post.title}
            />
            <Link to={`/posts/${post.slug}/view`}>
              <figcaption>
                <div className="h5">Xem chi tiết</div>
              </figcaption>
            </Link>
          </figure>
          <div className="content">
            <h4>
              <Link to={`/posts/${post.slug}/view`} className="text-truncate-3">
                {post.title}
              </Link>
            </h4>
            {/* <time>
              <i className="far fa-calendar mr-2"></i>
              <span>{moment.unix(post.created_at).format("LL")}</span>
            </time> */}
          </div>
        </article>
      ))}
    </div>
  );
};
