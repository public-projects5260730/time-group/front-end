import { useEffect, useState } from "react";

import { from } from "rxjs";
import { bannerApi } from "../../../apis";
import { useWindowWidth } from "../../../hooks";
import { IBanner } from "../../../types";
import { imageUrl } from "../../../utils";

export const PostViewBannerContainer = () => {
  const windowWidth = useWindowWidth();
  const [banners, setBanners] = useState<IBanner[]>();

  useEffect(() => {
    const subscription = from(
      bannerApi.findBanners({ positions: "post-view-right-sidebar" })
    ).subscribe(({ banners }) => {
      setBanners(banners || []);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  if (!banners?.length || windowWidth < 992) {
    return <></>;
  }

  return (
    <div className="banner-list">
      {banners.map((banner) => {
        if (banner?.link) {
          return (
            <a href={banner.link} key={banner?.id}>
              <img src={imageUrl(banner.image)} alt={banner.title} />
            </a>
          );
        }

        return (
          <img
            src={imageUrl(banner.image)}
            alt={banner.title}
            key={banner?.id}
          />
        );
      })}
    </div>
  );
};
