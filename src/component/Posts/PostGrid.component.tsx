import { Link } from "react-router-dom";
import { IPost } from "../../types";
import { imageUrl } from "../../utils";

export const PostGridComponent = ({ post }: { post: IPost }) => {
  return (
    <article className="post-grid-item post-item">
      <figure className="snap post-thumb">
        <img
          className="img-fluid"
          src={imageUrl(post.image)}
          alt={post.title}
        />
        <Link to={`/posts/${post.slug}/view`}>
          <figcaption>
            <div className="h5">Xem chi tiết</div>
          </figcaption>
        </Link>
      </figure>
      <div className="content">
        <h4>
          <Link to={`/posts/${post.slug}/view`} className="text-truncate-2">
            {post.title}
          </Link>
        </h4>
        <p className="text-truncate-2">{post.excerpt}</p>
        {/* <time>
          <i className="far fa-calendar mr-2"></i>
          <span>{moment.unix(post.created_at).format("LL")}</span>
        </time> */}
      </div>
    </article>
  );
};
