import { useEffect, useState } from "react";

import { from } from "rxjs";
import { postApi } from "../../../apis";
import { IPost } from "../../../types";
import { OverlayLoading } from "../../tools";
import { PostGridListComponent } from "../PostGridList.component";

export const RelatedPostsContainer = ({ slug }: { slug: string }) => {
  const [posts, setPosts] = useState<IPost[]>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const subscription = from(postApi.findRelatedPosts(slug)).subscribe(
      ({ posts }) => {
        setIsLoading(false);
        setPosts(posts || []);
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, [slug]);

  if (posts === undefined) {
    return <OverlayLoading />;
  }

  if (!posts?.length) {
    return <></>;
  }

  return (
    <div className="related-posts">
      <div className="container">
        <div className="card card-none">
          <div className="card-header">
            <h4 className="card-title title-border-left text-uppercase">
              Tin Liên quan
            </h4>
          </div>
          <div className="card-body">
            {!!isLoading && <OverlayLoading />}
            <PostGridListComponent posts={posts} />
          </div>
        </div>
      </div>
    </div>
  );
};
