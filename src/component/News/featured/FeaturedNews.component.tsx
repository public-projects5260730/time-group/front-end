import { useMemo, useRef } from "react";
import { useWindowWidth } from "../../../hooks";
import { IPost } from "../../../types";
import { PostGridComponent, PostListComponent } from "../../Posts";
import { CardNoData, OverlayLoading } from "../../tools";

import "./../../../resources/posts/featured-news.scss";

export const FeaturedNewsComponent = ({ posts }: { posts?: IPost[] }) => {
  const featuredRef = useRef<any>();
  const windowWidth = useWindowWidth();

  const { gridPosts, listPosts } = useMemo(() => {
    if (posts?.length) {
      const width =
        featuredRef?.current?.offsetWidth || Math.min(windowWidth, 1320);
      const offset = Math.floor(width / 400);
      const gridPosts = posts.slice(0, offset - 1);
      const listPosts = posts.slice(offset - 1);

      return { gridPosts, listPosts };
    }
    return { gridPosts: [], listPosts: [] };
  }, [posts, windowWidth]);

  if (posts === undefined) {
    return <OverlayLoading />;
  }

  if (!posts?.length) {
    return <></>;
  }

  return (
    <div className="featured" ref={featuredRef}>
      {gridPosts?.map((post) => (
        <PostGridComponent key={post.id} post={post} />
      ))}

      {!!listPosts?.length && (
        <div className="post-list">
          {listPosts?.map((post) => (
            <PostListComponent key={post.id} post={post} />
          ))}
        </div>
      )}
    </div>
  );
};
