import { useEffect, useState } from "react";

import { from } from "rxjs";
import { postApi } from "../../../apis";
import { IPost } from "../../../types";
import { FeaturedNewsComponent } from "./FeaturedNews.component";

export const FeaturedNewsContainer = () => {
  const [posts, setPosts] = useState<IPost[]>();

  useEffect(() => {
    const subscription = from(
      postApi.findPosts({
        categories: { slug: "tin-tuc" },
        featured: 1,
        limit: 5,
      })
    ).subscribe(({ posts }) => {
      setPosts(posts || []);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  return <FeaturedNewsComponent posts={posts} />;
};
