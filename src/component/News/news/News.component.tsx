import { IPaginate, IPost } from "../../../types";
import { PostGridListComponent } from "../../Posts";
import { CardNoData, OverlayLoading } from "../../tools";
import Pagination from "../../tools/Pagination";

export const NewsComponent = ({
  paginate,
  isLoading,
}: {
  paginate?: IPaginate<IPost> | null;
  isLoading?: boolean;
}) => {
  if (paginate === undefined) {
    return <OverlayLoading />;
  }
  if (paginate === null || !paginate?.data?.length) {
    return <CardNoData />;
  }

  return (
    <div className="card card-none">
      <div className="card-header">
        <h4 className="card-title title-border-left text-uppercase">
          Khám phá tin tức
        </h4>
      </div>
      <div className="card-body">
        {!!isLoading && <OverlayLoading />}
        <PostGridListComponent posts={paginate.data} />
        <div className="d-flex justify-content-center mt-5 mb-5">
          <Pagination paginate={paginate} />
        </div>
      </div>
    </div>
  );
};
