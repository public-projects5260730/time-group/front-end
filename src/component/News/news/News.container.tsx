import { useEffect, useState } from "react";

import { from } from "rxjs";
import { postApi } from "../../../apis";
import { useFilters } from "../../../hooks";
import { IPaginate, IPost } from "../../../types";
import { NewsComponent } from "./News.component";

export const NewsContainer = () => {
  const { filters } = useFilters();
  const [paginate, setPaginate] = useState<IPaginate<IPost> | null>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const { page } = { page: 1, ...filters };

    setIsLoading(true);
    const subscription = from(
      postApi.findPaginate({
        categories: { slug: "tin-tuc" },
        featured: 0,
        page,
        perPage: 12,
      })
    ).subscribe(({ paginate }) => {
      setPaginate(paginate || null);
      setIsLoading(false);
    });

    return () => {
      subscription.unsubscribe();
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters?.page]);

  return <NewsComponent paginate={paginate} isLoading={isLoading} />;
};
