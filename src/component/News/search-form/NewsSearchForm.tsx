import { useForm } from "react-hook-form";
import { useFilters } from "../../../hooks";

import "./../../../resources/posts/search-form.scss";

export const NewsSearchForm = () => {
  const { filters, pushFilters } = useFilters();

  const methods = useForm<{ search?: string }>({
    defaultValues: filters,
  });

  const { handleSubmit, register } = methods;

  const onSubmit = handleSubmit((formData) => {
    pushFilters({ ...formData, page: undefined }, "/news/search");
  });

  return (
    <form className="search-form" onSubmit={onSubmit}>
      <input
        className="form-control"
        placeholder="Tìm kiếm thông tin"
        {...register("search")}
      />
      <button type="submit">
        <i className="fas fa-search"></i>
      </button>
    </form>
  );
};
