import { useEffect, useState } from "react";

import { from } from "rxjs";
import { postApi } from "../../../apis";
import { useFilters } from "../../../hooks";
import { IPaginate, IPost } from "../../../types";
import { PostGridListComponent } from "../../Posts";
import { CardNoData, OverlayLoading } from "../../tools";
import Pagination from "../../tools/Pagination";

export const SearchNewsContainer = () => {
  const { filters } = useFilters();
  const [paginate, setPaginate] = useState<IPaginate<IPost> | null>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const subscription = from(
      postApi.findPaginate({
        ...filters,
        categories: { slug: "tin-tuc" },
        perPage: 12,
      })
    ).subscribe(({ paginate }) => {
      setPaginate(paginate || null);
      setIsLoading(false);
    });

    return () => {
      subscription.unsubscribe();
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters?.page, filters?.search]);

  if (paginate === undefined) {
    return <OverlayLoading />;
  }
  if (paginate === null || !paginate?.data?.length) {
    return <CardNoData />;
  }

  return (
    <div className="card card-none">
      <div className="card-header">
        <h4 className="card-title title-border-left text-uppercase">
          Tìm kiếm tin tức
        </h4>
      </div>
      <div className="card-body">
        {!!isLoading && <OverlayLoading />}
        <PostGridListComponent posts={paginate.data} />
        <div className="d-flex justify-content-center mt-5 mb-5">
          <Pagination paginate={paginate} />
        </div>
      </div>
    </div>
  );
};
