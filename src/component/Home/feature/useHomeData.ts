import { useEffect, useState } from "react";
import { companyApi, personnelApi, postApi } from "../../../apis";
import { ICompany, IPersonnel, Posts } from "../../../types";

export function useHomeData() {
  const [posts, setPosts] = useState<Posts[]>();
  const [companies, setCompanies] = useState<ICompany[]>();
  const [personnels, setPersonnels] = useState<IPersonnel[]>();

  const [postLoadingStatus, setPostLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [postError, setPostError] = useState<string>();

  const [companiesLoadingStatus, setCompaniesLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [companiesError, setCompaniesError] = useState<string>();

  const [personnelsLoadingStatus, setPersonnelsLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [personnelsError, setPersonnelsError] = useState<string>();

  useEffect(() => {
    postApi
      .findPosts({
        categories: {
          slug: "tin-tuc",
        },
        limit: 8,
      })
      .then((rs) => {
        if (rs.posts) {
          setPostLoadingStatus("done");
          setPosts(rs.posts);
          setPostError("post error");
        } else {
          setPostLoadingStatus("error");
          setPostError(rs.error || "");
        }
      })
      .catch((err) => {
        setPostLoadingStatus("error");
        setPostError("unknown error");
      });
  }, []);

  useEffect(() => {
    companyApi
      .getCompanies()
      .then((rs) => {
        if (rs.companies) {
          setCompaniesLoadingStatus("done");
          setCompanies(rs.companies);
        } else {
          setCompaniesLoadingStatus("error");

          setCompaniesError(rs.error || "");
        }
      })
      .catch((err) => {
        setCompaniesLoadingStatus("error");
        setCompaniesError("unknown error");
      });
  }, []);

  useEffect(() => {
    personnelApi
      .getPersonnels("")
      .then((rs) => {
        if (rs.personnels) {
          setPersonnelsLoadingStatus("done");
          setPersonnels(rs.personnels);
        } else {
          setPersonnelsLoadingStatus("error");
          setPersonnelsError(rs.error || "");
        }
      })
      .catch((err) => {
        setPersonnelsLoadingStatus("error");
        setPersonnelsError("unknown error");
      });
  }, []);

  const preButtonHanldeClick = () => {
    if (personnels) {
      const slideLenght = personnels.length;
      const lastPersonnel: IPersonnel = personnels[slideLenght - 1];
      personnels.pop();
      setPersonnels([lastPersonnel, ...personnels]);
    }
  };

  const nextButtonHanldeClick = () => {
    if (personnels) {
      const firstPersonnel: IPersonnel = personnels[0];
      personnels.shift();
      setPersonnels([...personnels, firstPersonnel]);
    }
  };

  return {
    posts,
    postLoadingStatus,
    postError,
    companies,
    companiesLoadingStatus,
    companiesError,
    personnels,
    personnelsLoadingStatus,
    personnelsError,
    preButtonHanldeClick,
    nextButtonHanldeClick,
  };
}
