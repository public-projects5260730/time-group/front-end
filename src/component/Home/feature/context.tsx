import { createContext } from "react";
import { ICompany, IPersonnel, Posts } from "../../../types";

type ContextType = {
  posts?: Posts[];
  postLoadingStatus: "loading" | "done" | "error";
  postError?: string;
  companies?: ICompany[];
  companiesLoadingStatus: "loading" | "done" | "error";
  companiesError?: string;
  personnels?: IPersonnel[];
  personnelsLoadingStatus: "loading" | "done" | "error";
  personnelsError?: string;
  preButtonHanldeClick: () => void;
  nextButtonHanldeClick: () => void;
};

export const PageHomeContext = createContext<ContextType>({
  posts: [],
  postLoadingStatus: "loading",
  companies: [],
  companiesLoadingStatus: "loading",
  personnels: [],
  personnelsLoadingStatus: "loading",
  preButtonHanldeClick: () => {
    //
  },
  nextButtonHanldeClick: () => {
    //
  },
});
