import React from "react";
import { Link } from "react-router-dom";
import { useWindowWidth } from "../../../../hooks";
import HomeMainStartupVentureItemComponent from "./HomeMainStartupVentureItem.component";

const HomeMainStartupVentureComponent = () => {
  const windowWidth = useWindowWidth();

  return (
    <div id="startup-venture" className="space_section">
      <div className="container">
        <div className="row">
          {windowWidth < 578 && (
            <div className="col-12 col-sm-4">
              <div className="logo-vertical">
                <img src="/assets/images/logo-vertical.svg" alt="Time Group" />
                <div className="text-a">
                  <Link className="animated-button" to="/venture-builder">
                    Tìm hiểu thêm
                  </Link>
                </div>
              </div>
            </div>
          )}
          <div className="col-12 col-sm-4">
            <HomeMainStartupVentureItemComponent
              imgAlt="Icon 04"
              imgLink="/assets/images/icon-04.png"
              title="Giúp startup Startup nhanh chóng hoàn thiện sản phẩm"
              paragraph="Timegroup cung cấp cùng lúc nhiều nguồn lực hỗ trợ startup như:"
              listBenefit={[
                "- Hệ thống labs công nghệ",
                "- Cố vấn công nghệ",
                "- Cố vấn chiến lược sản phẩm",
                "- Nguồn vốn đầu tư",
              ]}
            />
            <HomeMainStartupVentureItemComponent
              imgAlt="Icon 05"
              imgLink="/assets/images/icon-05.png"
              title="Giúp Startup xây dựng công ty tinh gọn và hiệu quả"
              paragraph="TIMEGROUP giúp startup khởi nghiệp tinh gọn với:"
              listBenefit={[
                "- Bộ máy công ty tinh gọn",
                "- Hệ thống quản trị và quản lý phù hợp, dễ điều hành",
              ]}
            />
          </div>
          {windowWidth >= 578 && (
            <div className="col-12 col-sm-4">
              <div className="logo-vertical">
                <img src="/assets/images/logo-vertical.svg" alt="Time Group" />
                <div className="text-a">
                  <Link className="animated-button" to="/venture-builder">
                    Tìm hiểu thêm
                  </Link>
                </div>
              </div>
            </div>
          )}
          <div className="col-12 col-sm-4">
            <HomeMainStartupVentureItemComponent
              imgAlt="Icon 06"
              imgLink="/assets/images/icon-06.png"
              paragraph="TIMEGROUP giúp startup:"
              listBenefit={[
                "- Kết nối, mở rộng các kênh marketing và bán hàng trên mạng lưới quan hệ của TimeGroup",
                "- Liên kết giữa các công ty trong mạng lưới công ty trong TimeGroup để nâng cao chất lượng CSKH",
              ]}
              title="Giúp Startup Phát triển hệ thống Sales-Marketing-CSKH"
            />

            <HomeMainStartupVentureItemComponent
              imgAlt="Icon 07"
              imgLink="/assets/images/icon-07.png"
              paragraph="TIMEGROUP mang đến cho Founder, CEO của startup cơ hội làm việc với:"
              listBenefit={[
                "- Các mentor am hiểu về khởi nghiệp sẽ chia sẻ kinh nghiệm thành công",
                "- Các chuyên gia về công nghệ blockchain, tài chính, bán hàng, marketing… giàu kinh nghiệm trên nhiều lĩnh vực sẽ tham gia đào tạo kỹ năng, chỉ dẫn cho nhân viên của startup",
              ]}
              title="Giúp Founder, CEO giữ lửa đam mê và ý chí khởi nghiệp"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeMainStartupVentureComponent;
