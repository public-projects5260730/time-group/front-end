import React from "react";

const HomeMainStartupVentureItemComponent = ({
  imgLink,
  imgAlt,
  title,
  paragraph,
  listBenefit,
}: {
  imgLink: string;
  imgAlt: string;
  title: string;
  paragraph: string;
  listBenefit: string[];
}) => {
  return (
    <div className="item-intro">
      <div className="row">
        <div className="col-2">
          <img src={imgLink} alt={imgAlt} />
        </div>
        <div className="col-10">
          <h5>{title}</h5>
          <p>{paragraph}</p>
          <ul>
            {listBenefit.map((item, index) => {
              return <li key={index}>{item}</li>;
            })}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default HomeMainStartupVentureItemComponent;
