import React from "react";
import { Link } from "react-router-dom";
import { SocialItemComponent } from "../../common";

const HomeMainTopComponent = () => {
  return (
    <div id="home-top">
      <div className="container">
        <div className="light2" />
        <div className="bfx_mod01" />
        <img className="abs_jj" src="/assets/images/rocket.png" alt="Rocket" />

        <article>
          <h3>Hệ sinh thái</h3>
          <h1>TimeGroup</h1>
          <p>
            Vườn ươm công nghệ hàng đầu Việt Nam trong lĩnh vực <br /> FinTech,
            MarTech và Blockchain
          </p>
          <p className="text-a">
            <Link to="/ecosystem" className="animated-button">
              <small>Tìm hiểu thêm</small>
            </Link>
          </p>
        </article>
        <ul className="fanpages">
          <SocialItemComponent
            iconSocial="fab fa-google"
            linkSocial="#"
            title="Google Plus"
          />
          <SocialItemComponent
            iconSocial="fab fa-youtube"
            linkSocial="#"
            title="Youtube"
          />
          <SocialItemComponent
            iconSocial="fab fa-linkedin"
            linkSocial="#"
            title="Linkedin"
          />
          <SocialItemComponent
            iconSocial="fab fa-telegram"
            linkSocial="#"
            title="Telegram"
          />
          <SocialItemComponent
            iconSocial="fab fa-facebook"
            linkSocial="#"
            title="Facebook"
          />
          <SocialItemComponent
            iconSocial="fab fa-twitter"
            linkSocial="#"
            title="Twitter"
          />
          <SocialItemComponent
            iconSocial="fab fa-instagram"
            linkSocial="#"
            title="Instagram"
          />
        </ul>
      </div>
    </div>
  );
};

export default HomeMainTopComponent;
