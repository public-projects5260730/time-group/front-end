import React, { useEffect, useState, useContext } from "react";
import { IPersonnel } from "../../../../types";
import { imgLinktoServer } from "../../../../utils";
import { PageHomeContext } from "../../feature";

const HomeMainTeamSlideComponent = ({
  personnels,
}: {
  personnels: IPersonnel[];
}) => {
  const constantIndex = 1;
  const [currentPersonnel, setCurrentPersonnel] = useState<{
    personnel: IPersonnel;
    index: number;
  }>();
  const { nextButtonHanldeClick, preButtonHanldeClick } = useContext(
    PageHomeContext
  );

  useEffect(() => {
    if (currentPersonnel && currentPersonnel.index < constantIndex) {
      preButtonHanldeClick();
    }

    if (currentPersonnel && currentPersonnel.index > constantIndex) {
      nextButtonHanldeClick();
    }
  }, [currentPersonnel]);

  return (
    <div className="team-slide__slide">
      <div className="team-slide__slide-container">
        {personnels.map((personnel, index) => {
          return (
            <div
              className="team-slide__slide-personnel"
              key={index}
              onClick={() => setCurrentPersonnel({ personnel, index })}
            >
              <div className="team-slide__slide-personnel-img">
                <img
                  src={`${imgLinktoServer}${personnel.image}`}
                  alt={personnel.name}
                  className={`team-slide__slide-personnel-img-item ${
                    index === 1 ? "active" : ""
                  }`}
                />
              </div>
              {index === 1 && (
                <div className="team-slide__slide-personnel-info">
                  <div className="slide-personnel-info__left">
                    {personnel.name}
                  </div>

                  <div className="slide-personnel-info__separate" />

                  <div className="slide-personnel-info__right">
                    <div className="slide-personnel-info__right-name">
                      {personnel.name}
                    </div>
                    <div className="slide-personnel-info__right-position">
                      {personnel.position}
                    </div>
                  </div>
                </div>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default HomeMainTeamSlideComponent;
