import React, { useContext } from "react";
import { PageHomeContext } from "../../feature";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/navigation";
import "../../../../resources/home/swiper.scss";

// import required modules
import { Navigation, Autoplay } from "swiper";
import { imageUrl } from "../../../../utils";

const HomeMainTeamContentComponent = () => {
  const { personnels, personnelsLoadingStatus, personnelsError } = useContext(
    PageHomeContext
  );

  if (personnelsLoadingStatus === "loading") {
    return <div className="">Loading...</div>;
  }

  if (personnelsLoadingStatus === "error") {
    return (
      <div className="">
        {personnelsError ? personnelsError : "Something went wrong..."}
      </div>
    );
  }

  if (!personnels || personnels.length === 0) {
    return <div className="">No data</div>;
  }

  return (
    <Swiper
      grabCursor={true}
      centeredSlides={true}
      navigation={true}
      modules={[Navigation, Autoplay]}
      className="personnels-swiper"
      loop={true}
      slidesPerView={1}
      spaceBetween={5}
      breakpoints={{
        768: {
          slidesPerView: 3,
        },
      }}
    >
      {personnels.map((personnel, index) => {
        return (
          <SwiperSlide key={index}>
            <div className="team-slide__slide-personnel">
              <div className="team-slide__slide-personnel-img">
                <img src={imageUrl(personnel.image)} alt={personnel.name} />
              </div>
              <div className="team-slide__slide-personnel-info">
                <div className="slide-personnel-info__left">
                  {personnel.name}
                </div>

                <div className="slide-personnel-info__separate" />

                <div className="slide-personnel-info__right">
                  <div className="slide-personnel-info__right-name">
                    {personnel.name}
                  </div>
                  <div className="slide-personnel-info__right-position">
                    {personnel.position}
                  </div>
                </div>
              </div>
            </div>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

export default HomeMainTeamContentComponent;
