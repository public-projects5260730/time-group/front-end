export { default as HomeMainTeamComponent } from "./HomeMainTeam.component";
export {
  default as HomeMainTeamContentComponent,
} from "./HomeMainTeamContent.component";
export {
  default as HomeMainTeamSlideComponent,
} from "./HomeMainTeamSlide.component";
