import React from "react";
import HomeMainTeamContentComponent from "./HomeMainTeamContent.component";

const HomeMainTeamComponent = () => {
  return (
    <div id="team" className="space_section">
      <div className="container">
        <hgroup className="title-main">
          <h2>Đội ngũ cố vấn</h2>
          <h6>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
            nonummy <br /> nibh euismod tincidunt ut laoreet dolore magna
            aliquam erat.
          </h6>
        </hgroup>
        <HomeMainTeamContentComponent />
      </div>
    </div>
  );
};

export default HomeMainTeamComponent;
