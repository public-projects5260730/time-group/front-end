import React from "react";

const HomeMainBenefitItemComponent = ({
  text1,
  text2,
  imgLink,
  imgAlt,
  paragraph,
}: {
  text1: string;
  text2?: string;
  imgLink: string;
  imgAlt: string;
  paragraph: string;
}) => {
  return (
    <div className="benefit-grid-block__benefits-grid-item nth_article">
      <div className="content">
        <article>
          <img src={imgLink} alt={imgAlt} />
          <h4>
            {text1} <br />
            {text2 ? text2 : ""}
          </h4>
          <p>{paragraph}</p>
          <p className="dot">
            <span />
            <span />
            <span />
          </p>
        </article>
      </div>
    </div>
  );
};

export default HomeMainBenefitItemComponent;
