import React from "react";
import { Link } from "react-router-dom";
import { useWindowWidth } from "../../../../hooks";
import HomeMainBenefitItemComponent from "./HomeMainBenefitItem.component";

const HomeMainBenefitComponent = () => {
  const windowWidth = useWindowWidth();

  return (
    <div id="benefit" className="space_section">
      <div className="container">
        <div className="row">
          <div className="col-12 col-sm-5">
            <hgroup className="title-main">
              <h2>
                <div className="h6">Lợi ích cho startup trong hệ sinh thái</div>
                <div className="h2">TimeGroup</div>
              </h2>
            </hgroup>
            <img
              className="img-fluid benefit__right-img"
              src="/assets/images/benefit.png"
              alt="Benefit Startup"
            />
          </div>

          <div className="col-12 col-sm-7">
            <div className="benefit-grid-block__benefits-grid">
              <HomeMainBenefitItemComponent
                imgAlt="Toan dien icon"
                imgLink="/assets/images/icon-08.png"
                paragraph="Các kế hoạch đầu tư sẽ dựa trên việc tiếp cận tổng thể, khảo sát thực tế, 
                trải nghiệm khách hàng, đánh giá tính khả thi cũng như khả năng thương mại."
                text1="Đầu tư vốn và phát triển"
                text2="nguồn lực toàn diện"
              />
              <HomeMainBenefitItemComponent
                imgAlt="Quan tri icon"
                imgLink="/assets/images/nang-luc-quan-tri-icon.png"
                paragraph="Thông qua việc có sẵn đội ngũ chuyên gia dẫn dắt startup thiết lập bộ máy backoffice, 
                quy trình vận hành, kết nối với những startup khác trong cùng hệ sinh thái để hỗ trợ lẫn nhau,..."
                text1="Nâng cao năng lực"
                text2="quản trị"
              />
              <HomeMainBenefitItemComponent
                imgAlt="Cong nghe icon"
                imgLink="/assets/images/nang-luc-cong-nghe-icon.png"
                paragraph="Thông qua các nguồn lực như: các IT lab, kỹ sư phần mềm, 
                đội ngũ phát triển sản phẩm, bộ máy marketing của venture builder, chuyên gia công nghệ…"
                text1="Nâng cao năng lực."
                text2="công nghệ"
              />
              <HomeMainBenefitItemComponent
                imgAlt="Kinh doanh icon"
                imgLink="/assets/images/nang-luc-kinh-doanh-icon.png"
                paragraph="Thông qua các nguồn lực như: các IT lab, kỹ sư phần mềm, 
                đội ngũ phát triển sản phẩm, bộ máy marketing của venture builder, chuyên gia thuộc các lĩnh vực liên quan…"
                text1="Nâng cao năng lực"
                text2="kinh doanh"
              />
            </div>
          </div>
        </div>

        {windowWidth < 768 && (
          <div className="row">
            <div className="benefit__ecosystem">
              <button className="benefit__ecosystem-button">
                <Link to="/ecosystem" className="benefit__ecosystem-link">
                  Tham gia hệ sinh thái
                </Link>
              </button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
export default HomeMainBenefitComponent;
