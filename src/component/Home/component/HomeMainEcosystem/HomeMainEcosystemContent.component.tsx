import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { imgLinktoServer } from "../../../../utils";
import { PageHomeContext } from "../../feature";

const HomeMainEcosystemContentComponent = () => {
  const { companies, companiesLoadingStatus, companiesError } = useContext(
    PageHomeContext
  );

  if (companiesLoadingStatus === "loading") {
    return <div className="">Loading...</div>;
  }

  if (companiesLoadingStatus === "error") {
    return (
      <div className="">
        {companiesError ? companiesError : "Something went wrong..."}
      </div>
    );
  }

  if (!companies || companies.length === 0) {
    return <div className="">No data</div>;
  }

  return (
    <div className="row">
      {companies.map((company, index) => {
        return (
          <div className="col-4 col-md-5ths col-lg-5ths" key={index}>
            <Link to="#">
              <img
                src={`${imgLinktoServer}${company.logo}`}
                alt={company.name}
              />
            </Link>
          </div>
        );
      })}
      {/* <div className="col-4 col-md-5ths col-lg-5ths">
        <Link to="#" />
      </div>
      <div className="col-4 col-md-5ths col-lg-5ths">
        <Link to="#" />
      </div>
      <div className="col-4 col-md-5ths col-lg-5ths">
        <Link to="#" />
      </div>
      <div className="col-4 col-md-5ths col-lg-5ths">
        <Link to="#" />
      </div> */}
    </div>
  );
};

export default HomeMainEcosystemContentComponent;
