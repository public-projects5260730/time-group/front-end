import React, { useState, useContext } from "react";
import { Modal } from "react-bootstrap";
import { ICompany } from "../../../../types";
import { imageUrl } from "../../../../utils";
import { CompanyViewComponent } from "../../../Ecosystem/View";
import { OverlayLoading } from "../../../tools";
import { PageHomeContext } from "../../feature";

const HomeMainEcosystemComponent = () => {
  const [selectedCompany, setSelectedCompany] = useState<ICompany>();
  const { companies, companiesLoadingStatus } = useContext(PageHomeContext);

  if (companiesLoadingStatus === "loading") {
    return <OverlayLoading />;
  }

  if (!companies?.length) {
    return <></>;
  }

  return (
    <div id="ecosystem" className="space_section">
      <div className="container">
        <hgroup className="title-main">
          <h2>
            <div className="h6">TimeGroup</div>
            <div className="h2">TimeGroup</div>
          </h2>
        </hgroup>
        <div className="company-list">
          {companies
            .filter(({ logo, image }) => !!logo?.length || !!image?.length)
            .map((company) => (
              <div
                className="cursor-pointer"
                key={company.id}
                onClick={() => setSelectedCompany(company)}
              >
                <img
                  src={imageUrl(company?.logo || company?.image)}
                  alt={company.name}
                />
              </div>
            ))}
        </div>
        {!!selectedCompany && (
          <Modal
            show={true}
            onHide={() => setSelectedCompany(undefined)}
            centered
            animation={false}
            className="company-view"
          >
            <Modal.Header closeButton />
            <Modal.Body>
              <CompanyViewComponent company={selectedCompany} />
            </Modal.Body>
          </Modal>
        )}
      </div>
    </div>
  );
};

export default HomeMainEcosystemComponent;
