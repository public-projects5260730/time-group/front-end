import React from "react";
import { Link } from "react-router-dom";

const HomeMainBoxInfoComponent = () => {
  return (
    <div id="box-infor">
      <div className="container">
        <div className="row">
          <div className="col-3 col-sm-2">
            <img
              className="img-fluid"
              src="/assets/images/block-timegroup.svg"
              alt="Block TimeGroup"
            />
          </div>
          <div className="col-9 col-sm-6">
            <p className="desc">
              Một mô hình tổ chức "vườn ươm công nghệ" hoạt động theo hình thức
              đầu tư xây dựng các công ty startup bằng cách{" "}
              <strong>
                cung cấp vốn đồng thời sử dụng những nguồn lực có sẵn của tổ
                chức
              </strong>{" "}
              để giúp startup hiện thực hóa các ý tưởng mới.
            </p>
          </div>
          <div className="col-12 col-sm-4">
            <article>
              <h2>Venture Builder</h2>
              <h3>Cơ hội phát triển toàn diện cho startup</h3>
              <p className="text-a">
                <Link className="animated-button" to="/venture-builder">
                  <small>Tìm hiểu thêm</small>
                </Link>
              </p>
            </article>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeMainBoxInfoComponent;
