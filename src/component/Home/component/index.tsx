export * from "./HomeMainNews";
export * from "./HomeMainBenefit";
export * from "./HomeMainStartupVenture";
export * from "./HomeMainTeam";
export * from "./HomeMainEcosystem";
export { default as HomeMainTopComponent } from "./HomeMainTop.component";
export {
  default as HomeMainBoxInfoComponent,
} from "./HomeMainBoxInfo.component";

export {
  default as HomeMainVentureComponent,
} from "./HomeMainVenture.component";
