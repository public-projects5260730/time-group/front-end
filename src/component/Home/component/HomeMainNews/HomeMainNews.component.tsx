import React from "react";
import HomeMainNewsPostsComponent from "./HomeMainNewsPosts.component";

const HomeMainNewsComponent = () => {
  return (
    <div id="news" className="space_section">
      <div className="container">
        <hgroup className="title-main">
          <h3>Tin tức</h3>
        </hgroup>
        <HomeMainNewsPostsComponent />
      </div>
    </div>
  );
};

export default HomeMainNewsComponent;
