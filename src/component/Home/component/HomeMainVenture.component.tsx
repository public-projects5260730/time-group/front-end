import React from "react";

const HomeMainVentureComponent = () => {
  return (
    <div id="venture" className="space_section">
      <div className="container">
        <hgroup className="title-main">
          <h2>
            <div className="h6">Venture Builder</div>
            <div className="h2">TimeGroup</div>
          </h2>
        </hgroup>

        <div className="venture-grid__block">
          <div className="venture-grid__block-item">
            <div className="content">
              <article>
                <img src="/assets/images/icon-01.png" alt="Icon 01" />
                <h4>Venture Capital</h4>
                <h6>Quỹ vốn đầu tư Startup</h6>
                <p>
                  Cung cấp vốn đầu tư mạo hiểm và kêu gọi nhà đầu tư thiên thần
                  cho startup.
                </p>
              </article>
            </div>
          </div>
          <div className="venture-grid__block-item">
            <div className="content">
              <article>
                <img src="/assets/images/icon-02.png" alt="Icon 02" />
                <h4>Incubator</h4>
                <h6>Vườn ươm Startup</h6>
                <p>
                  Hỗ trợ tư vấn giúp các startup "đổi mới" và đột phá về công
                  nghệ
                </p>
              </article>
            </div>
          </div>
          <div className="venture-grid__block-item">
            <div className="content">
              <article>
                <img src="/assets/images/icon-03.png" alt="Icon 03" />
                <h4>Accelerator</h4>
                <h6>Tăng tốc Startup</h6>
                <p>
                  Giúp các startup phát triển nhanh và mạnh trong thời gian
                  ngắn.
                </p>
              </article>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomeMainVentureComponent;
