import React from "react";
import {
  HomeMainBenefitComponent,
  HomeMainBoxInfoComponent,
  HomeMainEcosystemComponent,
  HomeMainNewsComponent,
  HomeMainStartupVentureComponent,
  HomeMainTopComponent,
  HomeMainVentureComponent,
} from "./component";
import { PageHomeContext, useHomeData } from "./feature";

const HomeContainer = () => {
  const pageData = useHomeData();

  return (
    <main>
      <PageHomeContext.Provider value={pageData}>
        <HomeMainTopComponent />
        <HomeMainBoxInfoComponent />
        <HomeMainVentureComponent />
        <HomeMainStartupVentureComponent />
        <HomeMainEcosystemComponent />
        <HomeMainBenefitComponent />
        <HomeMainNewsComponent />
      </PageHomeContext.Provider>
    </main>
  );
};

export default HomeContainer;
