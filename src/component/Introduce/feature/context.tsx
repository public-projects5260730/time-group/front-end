import { createContext } from "react";
import { IPersonnel, Posts } from "../../../types";

type ContextType = {
  posts?: Posts[];
  postLoadingStatus: "loading" | "done" | "error";
  postError?: string;
  personnels?: IPersonnel[];
  personnelsLoadingStatus: "loading" | "done" | "error";
  personnelsError?: string;
};

export const PageIntroduceContext = createContext<ContextType>({
  posts: [],
  postLoadingStatus: "loading",
  personnels: [],
  personnelsLoadingStatus: "loading",
});
