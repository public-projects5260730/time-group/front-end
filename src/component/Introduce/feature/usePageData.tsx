import { useEffect, useState } from "react";
import { personnelApi, postApi } from "../../../apis";
import { IPersonnel, Posts } from "../../../types";

export const usePageData = () => {
  const [posts, setPosts] = useState<Posts[]>();
  const [personnels, setPersonnels] = useState<IPersonnel[]>();

  const [postLoadingStatus, setPostLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [postError, setPostError] = useState<string>();

  const [personnelsLoadingStatus, setPersonnelsLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [personnelsError, setPersonnelsError] = useState<string>();

  useEffect(() => {
    postApi
      .findPosts({
        categories: {
          slug: "tin-tuc",
        },
        limit: 8,
      })
      .then((rs) => {
        if (rs.posts) {
          setPostLoadingStatus("done");
          setPosts(rs.posts);
          setPostError("post error");
        } else {
          setPostLoadingStatus("error");
          setPostError(rs.error || "");
        }
      })
      .catch((err) => {
        setPostLoadingStatus("error");
        setPostError("unknown error");
      });
  }, []);

  useEffect(() => {
    personnelApi
      .getPersonnels("?limit=6")
      .then((rs) => {
        if (rs.personnels) {
          setPersonnelsLoadingStatus("done");
          setPersonnels(rs.personnels);
        } else {
          setPersonnelsLoadingStatus("error");
          setPersonnelsError(rs.error || "");
        }
      })
      .catch((err) => {
        setPersonnelsLoadingStatus("error");
        setPersonnelsError("unknown error");
      });
  }, []);

  return {
    posts,
    postLoadingStatus,
    postError,
    personnels,
    personnelsLoadingStatus,
    personnelsError,
  };
};
