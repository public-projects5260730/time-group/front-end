import React from "react";
import {
  IntroduceMainAboutComponent,
  IntroduceMainNewsComponent,
  IntroduceMainTopComponent,
} from "./component";
import { PageIntroduceContext, usePageData } from "./feature";

const IntroduceConatiner = () => {
  const pageData = usePageData();

  return (
    <PageIntroduceContext.Provider value={pageData}>
      <main>
        <IntroduceMainTopComponent />
        <IntroduceMainAboutComponent />
        <IntroduceMainNewsComponent />
      </main>
    </PageIntroduceContext.Provider>
  );
};

export default IntroduceConatiner;
