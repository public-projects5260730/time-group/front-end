import React, { useContext } from "react";
import { imgLinktoServer } from "../../../../utils";
import { NewsBottomComponentItem } from "../../../common";
import { PageIntroduceContext } from "../../feature";

const IntroduceMainNewsPostsComponent = () => {
  const { posts, postLoadingStatus, postError } = useContext(
    PageIntroduceContext
  );

  if (postLoadingStatus === "loading") {
    return <div className="container__main">Loading...</div>;
  }

  if (postLoadingStatus === "error") {
    return (
      <div className="container__main error">
        {postError ? postError : "Unknown error"}
      </div>
    );
  }

  if (!posts || posts.length === 0) {
    return <div className="container__main">Posts undefined</div>;
  }

  return (
    <div className="post-grid__block">
      {posts.map((post) => {
        return (
          <NewsBottomComponentItem
            postImgAlt={post.slug}
            postImgLink={`${imgLinktoServer}${post.image}`}
            postLink={`/posts/${post.slug}/view`}
            postTitle={post.title}
            shortPostParagraph={post.excerpt}
            key={post.id}
          />
        );
      })}
    </div>
  );
};

export default IntroduceMainNewsPostsComponent;
