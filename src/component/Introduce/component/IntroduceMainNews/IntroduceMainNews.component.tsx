import React from "react";
import IntroduceMainNewsPostsComponent from "./IntroduceMainNewsPosts.component";

const HomeMainNewsComponent = () => {
  return (
    <div id="news" className="space_section">
      <div className="container">
        <hgroup className="title-main">
          <h3>Tin tức</h3>
        </hgroup>
        <IntroduceMainNewsPostsComponent />
      </div>
    </div>
  );
};

export default HomeMainNewsComponent;
