export {
  default as IntroduceMainTeamComponent,
} from "./IntroduceMainTeam.component";
export {
  default as IntroduceMainTeamContentComponent,
} from "./IntroduceMainTeamContent.component";
