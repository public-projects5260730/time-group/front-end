import React, { useContext } from "react";
import { imgLinktoServer } from "../../../../utils";
import { PageIntroduceContext } from "../../feature";

const IntroduceMainTeamContentComponent = () => {
  const { personnels, personnelsLoadingStatus, personnelsError } = useContext(
    PageIntroduceContext
  );

  if (personnelsLoadingStatus === "loading") {
    return <div className="">Loading...</div>;
  }

  if (personnelsLoadingStatus === "error") {
    return (
      <div className="">
        {personnelsError ? personnelsError : "Something went wrong..."}
      </div>
    );
  }

  if (!personnels || personnels.length === 0) {
    return <div className="">No data</div>;
  }
  return (
    <div className="team-grid__block">
      {personnels.map((personnel, index) => {
        return (
          <div className="team-grid__block-item" key={index}>
            <div className="content">
              <div className="team-grid__block-item-personnel">
                <article>
                  <div className="team-grid__block-item-top">
                    <img
                      src={`${imgLinktoServer}${personnel.image}`}
                      alt={personnel.name}
                      className="team-grid__block-item-top-img"
                    />
                  </div>
                  <div className="team-grid__block-item-info">
                    <div className="team-grid__block-item-info-name">
                      {personnel.name}
                    </div>
                    <div className="team-grid__block-item-info-position">
                      {personnel.position}
                    </div>
                  </div>
                </article>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default IntroduceMainTeamContentComponent;
