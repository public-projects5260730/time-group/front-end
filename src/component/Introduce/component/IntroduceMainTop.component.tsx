import React from "react";

const IntroduceMainTopComponent = () => {
  return (
    <div id="introduce-main__top">
      <div className="container">
        <div className="introduce-main__top-logo-background" />

        <article>
          <h2 className="text-white">Hệ sinh thái đầu tư công nghệ</h2>
          <h1>Giới thiệu TimeGroup</h1>
        </article>
      </div>
    </div>
  );
};

export default IntroduceMainTopComponent;
