import React from "react";
import { Link } from "react-router-dom";

const IntroduceMainAboutComponent = () => {
  return (
    <div id="introduce__about" className="space_section">
      <div className="container">
        <div className="about__target">
          <div className="about__target-img">
            <img src="/assets/images/Group 48095987.png" alt="abc" />
          </div>
          <div className="about__target-paragraph">
            <p>
              Chúng tôi đầu tư công nghệ cho các ý tưởng kinh doanh tiềm năng và
              cung cấp những nguồn lực cần thiết sẵn có trong mạng lưới của tập
              đoàn cùng sự dẫn dắt của các chuyên gia giàu kinh nghiệm để giúp
              các nhà khởi nghiệp xây dựng công ty tinh gọn, hoạt động hiệu quả,
              tăng trưởng tốt.​
            </p>
          </div>
        </div>

        <div className="about-grid__block">
          <div className="about-grid__block-item">
            <div className="content">
              <article>
                <img src="/assets/images/su-menh-icon.png" alt="su menh" />
                <h3 className="text-uppercase mt-60 mb-32">Sứ mệnh</h3>
                <p>
                  Xây dựng và phát triển những công ty công nghệ tạo ra giá trị
                  kinh tế và thúc đẩy sự phát triển của xã hội.​
                </p>
              </article>
            </div>
          </div>
          <div className="about-grid__block-item">
            <div className="content">
              <article>
                <img src="/assets/images/tam-nhin-icon.png" alt="Icon 01" />
                <h3 className="text-uppercase mt-60 mb-32">Tầm nhìn</h3>
                <p>
                  Trở thành một đơn vị cung cấp môi trường phát triển lý tưởng
                  nhất cho các nhà khởi nghiệp và không ngừng mở rộng chia sẻ,
                  hợp tác, gia tăng các nguồn lực để phát triển công nghệ
                  blockchain, fintech, martech.​
                </p>
              </article>
            </div>
          </div>
          <div className="about-grid__block-item">
            <div className="content">
              <article>
                <img
                  src="/assets/images/gia-tri-cot-loi-icon.png"
                  alt="Icon 01"
                />
                <h3 className="text-uppercase mt-60 mb-32">Giá trị cốt lõi</h3>
                <ul>
                  <li>
                    <strong>MINH BẠCH</strong> - Mọi hoạt động của công ty
                  </li>
                  <li>
                    <strong>REMOTE HOÁ</strong> - Phát triển công nghệ nhằm hỗ
                    trợ con người hoàn thành công việc dễ dàng hơn, hiệu quả hơn
                  </li>
                  <li>
                    <strong>CLOUD HOÁ</strong> - Chia sẻ nguồn lực
                  </li>
                  <li>
                    <strong>THẬT</strong> - Đề cao phẩm chất trung thực của con
                    người
                  </li>
                </ul>
              </article>
            </div>
          </div>
        </div>

        <div className="about__more">
          <Link to="/ecosystem">
            <button className="about__more-button">
              <img
                src="/assets/images/Buildings.png"
                alt="Icon building"
                className="about__more-button-img"
              />
              <p>Các công ty trong hệ sinh thái TimeGroup</p>
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default IntroduceMainAboutComponent;
