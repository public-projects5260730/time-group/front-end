import { useState } from "react";
import { useForm } from "react-hook-form";
import { contactApi } from "../../apis";
import { ISendMessageInputs } from "../../types";
import { toast } from "react-toastify";

export const ContactSendMessageForm = () => {
  const [isSubmitting, setIsSubmitting] = useState(false);

  const methods = useForm<ISendMessageInputs>();

  const {
    handleSubmit,
    register,
    formState: { errors },
    setError,
  } = methods;

  const onSubmit = handleSubmit((formData) => {
    if (isSubmitting) {
      return;
    }
    setIsSubmitting(true);

    contactApi.sendMessage(formData).then(({ successfully, errors, error }) => {
      setIsSubmitting(false);
      if (errors) {
        Object.entries(errors).map(([field, message]) => {
          return setError(field as keyof ISendMessageInputs, {
            type: "manual",
            message,
          });
        });
      } else if (error?.length) {
        toast.error(error);
      }

      if (successfully) {
        toast.success("Successfully");
      }
    });
  });

  return (
    <div className="send-message">
      <h4 className="title">
        Bạn muốn tìm hiểu thêm về hệ sinh thái đầu tư công nghệ Timegroup?
      </h4>
      <div className="description">
        Đừng ngần ngại liên hệ với chúng tôi qua mẫu của chúng tôi dưới đây.
      </div>

      <form onSubmit={onSubmit}>
        <div className="row form-group">
          <div className="input-group">
            <span className="input-group-text">From:</span>
            <input
              type="email"
              className={`form-control${errors?.email ? " is-invalid" : ""}`}
              placeholder="email@gmail.com"
              {...register("email", { required: "Email is required." })}
            />
          </div>
        </div>

        <div className="row form-group">
          <div className="input-group">
            <span className="input-group-text">Nội dung:</span>
            <textarea
              className={`form-control${errors?.message ? " is-invalid" : ""}`}
              placeholder="TimeGroup có thể giúp gì cho bạn?"
              {...register("message", { required: "Email is required." })}
              rows={5}
            />
          </div>
        </div>

        <div className="text-center">
          <button type="submit" className="btn btn-primary">
            {isSubmitting && (
              <i className="fa fa-circle-o-notch fa-spin mr-2"></i>
            )}
            Gửi tin nhắn
          </button>
        </div>
      </form>
    </div>
  );
};
