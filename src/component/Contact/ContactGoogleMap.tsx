export const ContactGoogleMap = () => {
  return (
    <div className="google-map">
      <h1 className="title">Liên Hệ</h1>
      <div className="card">
        <div className="card-body">
          <div className="info">
            <div>
              <img src="/assets/images/v-map.png" alt="address" />
              <strong>Địa chỉ</strong>
              <span>V6B – 12A – Văn Phú – Hà Đông – Hà Nội</span>
            </div>

            <div>
              <img src="/assets/images/v-envelope.png" alt="Email" />
              <strong>Email</strong>
              <span>infor@timegroup.com</span>
            </div>

            <div>
              <img src="/assets/images/v-phone.png" alt="Phone" />
              <strong>Điện thoại</strong>
              <span>0589108963</span>
            </div>
          </div>
          <iframe
            title="Time Group"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d658.6540593371712!2d105.76934955157432!3d20.95612854510835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313453f8a6f053a5%3A0xa292fe08757ec500!2sTime%20Tech!5e0!3m2!1svi!2s!4v1675303425799!5m2!1svi!2s"
            width="100%"
            height="600px"
            referrerPolicy="no-referrer-when-downgrade"
          ></iframe>
        </div>
      </div>
    </div>
  );
};
