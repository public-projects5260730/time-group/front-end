import React from "react";
import {
  VentureBuilderMainAboutComponent,
  VentureBuilderMainBenefitComponent,
  VentureBuilderMainNewsComponent,
  VentureBuilderMainTopComponent,
} from "./component";
import { PageVentureBuilderContext, usePageData } from "./feature";

const VentureBuilderContainer = () => {
  const pageData = usePageData();

  return (
    <PageVentureBuilderContext.Provider value={pageData}>
      <main>
        <VentureBuilderMainTopComponent />
        <VentureBuilderMainAboutComponent />
        <VentureBuilderMainBenefitComponent />
        <VentureBuilderMainNewsComponent />
      </main>
    </PageVentureBuilderContext.Provider>
  );
};

export default VentureBuilderContainer;
