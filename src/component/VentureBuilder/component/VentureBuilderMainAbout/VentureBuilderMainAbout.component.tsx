import VentureBuilderMainAboutDropdown from "./VentureBuilderMainAboutDropdown";

const VentureBuilderMainAboutComponent = () => {
  return (
    <div id="venture-builder__about" className="space_section">
      <div className="container">
        <VentureBuilderMainAboutDropdown />
        <div className="venture-builder__about-grid">
          <div className="venture-builder__about-grid-text">
            <h2>
              <div className="h4">timegroup</div>
              <div className="h2">VENTURE BUILDER</div>
            </h2>
          </div>
          <div className="venture-builder-about-grid__block">
            <div className="venture-builder-about-grid__block-item">
              <div className="content">
                <article>
                  <div className="venture-builder-about-grid__block-item-img">
                    <img src="/assets/images/image 1.png" alt="coin" />
                  </div>
                  <div className="venture-builder-about-grid__block-item-paragraph">
                    <h3 className="text-uppercase h4">
                      VENTURE BUILDER đầu tư vốn và phát triển nguồn lực toàn
                      diện
                    </h3>
                    <p>
                      Cho các công ty startup ở mọi cấp độ (seed, series A…).
                      Các kế hoạch đầu tư sẽ dựa trên việc tiếp cận tổng thể,
                      khảo sát thực tế, trải nghiệm khách hàng, đánh giá tính
                      khả thi cũng như khả năng thương mại.
                    </p>
                  </div>
                </article>
              </div>
            </div>
            <div className="venture-builder-about-grid__block-item">
              <div className="content">
                <article>
                  <div className="venture-builder-about-grid__block-item-img">
                    <img
                      src="/assets/images/Rectangle 23018.png"
                      alt="hand with people"
                    />
                  </div>
                  <div className="venture-builder-about-grid__block-item-paragraph">
                    <h3 className="text-uppercase h4">
                      VENTURE BUILDER nâng cao năng lực quản trị
                    </h3>
                    <p>
                      Cho startup thông qua việc có sẵn đội ngũ chuyên gia dẫn
                      dắt startup thiết lập bộ máy back office, quy trình vận
                      hành, kết nối với những startup khác trong cùng hệ sinh
                      thái để hỗ trợ lẫn nhau,... ​
                    </p>
                  </div>
                </article>
              </div>
            </div>
            <div className="venture-builder-about-grid__block-item">
              <div className="content">
                <article>
                  <div className="venture-builder-about-grid__block-item-img">
                    <img
                      src="/assets/images/Rectangle 23019.png"
                      alt="hand-with-earth"
                    />
                  </div>
                  <div className="venture-builder-about-grid__block-item-paragraph">
                    <h3 className="text-uppercase h4">
                      VENTURE BUILDER nâng cao năng lực công nghệ
                    </h3>
                    <p>
                      Cho startup thông qua các nguồn lực như: các IT lab, kỹ sư
                      phần mềm, đội ngũ phát triển sản phẩm, bộ máy marketing
                      của VB, chuyên gia thuộc các lĩnh vực liên quan…​
                    </p>
                  </div>
                </article>
              </div>
            </div>
            <div className="venture-builder-about-grid__block-item">
              <div className="content">
                <article>
                  <div className="venture-builder-about-grid__block-item-img">
                    <img
                      src="/assets/images/Rectangle 23020.png"
                      alt="hand with chart"
                    />
                  </div>
                  <div className="venture-builder-about-grid__block-item-paragraph">
                    <h3 className="text-uppercase h4">
                      VENTURE BUILDER nâng cao năng lực kinh doanh
                    </h3>
                    <p>
                      Cho startup thông qua các nguồn lực như: các IT lab, kỹ sư
                      phần mềm, đội ngũ phát triển sản phẩm, bộ máy marketing
                      của VB, chuyên gia thuộc các lĩnh vực liên quan...
                    </p>
                  </div>
                </article>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VentureBuilderMainAboutComponent;
