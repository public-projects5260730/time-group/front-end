import React, { useContext } from "react";
import { raw } from "../../../../utils";
import { PageVentureBuilderContext } from "../../feature";

const VentureBuilderMainAboutDropdown = () => {
  const { aboutPosts, aboutPostLoadingStatus, aboutPostError } = useContext(
    PageVentureBuilderContext
  );

  if (aboutPostLoadingStatus === "loading") {
    return <div className="container__main">Loading...</div>;
  }

  if (aboutPostLoadingStatus === "error") {
    return (
      <div className="container__main error">
        {aboutPostError ? aboutPostError : "Unknown error"}
      </div>
    );
  }

  if (!aboutPosts || aboutPosts.length === 0) {
    return <div className="container__main">Posts undefined</div>;
  }

  return (
    <div className="venture-builder__about-dropdowns">
      {aboutPosts.map(({ title, body }, index) => (
        <div className="venture-builder__about-dropdown-item" key={index}>
          <button className="venture-builder__about-dropdown-button">
            <p className="venture-builder__about-dropdown-button-paragraph">
              {title}
            </p>
            <i className="fa fa-chevron-down venture-builder__about-dropdown-button-icon" />
          </button>
          <div className="venture-builder__about-dropdown-content">
            <p className="venture-builder__about-dropdown-content-paragraph">
              {raw(body)}
            </p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default VentureBuilderMainAboutDropdown;
