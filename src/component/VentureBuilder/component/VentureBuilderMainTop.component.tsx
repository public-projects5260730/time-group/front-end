import React from "react";

const VentureBuilderMainTopComponent = () => {
  return (
    <div id="venture-builder-main__top">
      <div className="container">
        <div className="venture-builder-main__top-logo-background" />

        <article>
          <h1>venture builder</h1>
        </article>
      </div>
    </div>
  );
};

export default VentureBuilderMainTopComponent;
