import React from "react";

const VentureBuilderMainBenefitComponent = () => {
  return (
    <div id="venture-builder__benefit">
      <div className="venture-builder__benefit-background" />
      <div className="container">
        <div className="venture-builder__benefit-text">
          <h2>
            <div className="h4">Startup nhận được gì</div>
            <div className="h2">Venture builder</div>
          </h2>
        </div>
        <div className="venture-builder__benefit-content">
          <div className="venture-builder__benefit-content-item">
            <div className="venture-builder__benefit-content-img">
              <img
                src="/assets/images/hoan-thien-san-pham-icon.png"
                alt="Nhanh-chong-hoan-thien"
              />
            </div>
            <div className="venture-builder__benefit-content-paragraph">
              <h4>Nhanh chóng​ hoàn thiện sản phẩm​</h4>
              <p>
                cho startup thông qua việc có sẵn đội ngũ chuyên gia dẫn dắt
                startup thiết lập bộ máy back office, quy trình vận hành, kết
                nối với những startup khác trong cùng hệ sinh thái để hỗ trợ lẫn
                nhau,... ​
              </p>
              <div className="venture-builder__benefit-content-img" />
            </div>
          </div>

          <div className="venture-builder__benefit-content-item">
            <div className="venture-builder__benefit-content-img" />

            <div className="venture-builder__benefit-content-paragraph">
              <h4>Xây dựng công ty​ tinh gọn – hiệu quả​</h4>
              <p>
                Để kinh doanh thành công, startup không chỉ cần những sản phẩm
                thỏa mãn ý tưởng, đam mê của founder, mà còn cần phải có một bộ
                máy tiết kiệm mà vẫn đầy đủ và hiệu quả, không quá mất thời gian
                vào các vấn đề điều hành. Venture builder giúp startup xây dựng
                công ty có đầy đủ bộ máy cơ bản của một doanh nghiệp để phát
                triển kinh doanh.​
              </p>
            </div>
            <div className="venture-builder__benefit-content-img">
              <img
                src="/assets/images/cong-ty-tinh-gon-icon.png"
                alt="Nhanh-chong-hoan-thien"
              />
            </div>
          </div>

          <div className="venture-builder__benefit-content-item">
            <div className="venture-builder__benefit-content-img">
              <img
                src="/assets/images/phat-trien-he-thong-icon.png"
                alt="Nhanh-chong-hoan-thien"
              />
            </div>
            <div className="venture-builder__benefit-content-paragraph">
              <h4>Phát triển hệ thống​ Sales & Marketing</h4>
              <p>
                Để khởi tạo công ty thành công, startup không chỉ cần những sản
                phẩm thỏa mãn ý tưởng, đam mê của founder, mà còn cần phải có
                một định hướng phát triển đúng đắn. Venture builder sẽ hỗ trợ
                startup xây dựng chiến lược kinh doanh phù hợp, để kết nối với
                các nhà đầu tư để phát triển doanh nghiệp.
              </p>
            </div>
            <div className="venture-builder__benefit-content-img" />
          </div>

          <div className="venture-builder__benefit-content-item">
            <div className="venture-builder__benefit-content-img" />
            <div className="venture-builder__benefit-content-paragraph">
              <h4>Giữ lửa đam mê và ý chí khởi nghiệp</h4>
              <p>
                Quá trình khởi nghiệp rất nhiều thách thức có thể khiến các
                Founder – CEO nản lòng và mất ý chí, vì vậy luôn cần có sự giúp
                đỡ tháo gỡ khó khăn. Venture builder có các lãnh đạo đã xây dựng
                công ty thành công, giàu kinh nghiệm và am hiểu về khởi nghiệp.
                Họ sẽ giúp các CEO startup giữ vững niềm đam mê kinh doanh, họ
                cố vấn và đào tạo cho CEO, có thể trực tiếp tham gia giải quyết
                khó khẳn của startup.​
              </p>
            </div>
            <div className="venture-builder__benefit-content-img">
              <img
                src="/assets/images/giu-lua-dam-me-icon.png"
                alt="Nhanh-chong-hoan-thien"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VentureBuilderMainBenefitComponent;
