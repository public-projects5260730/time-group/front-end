import React from "react";
import VentureBuilderMainNewsPostsComponent from "./VentureBuilderMainNewsPosts.component";

const VentureBuilderMainNewsComponent = () => {
  return (
    <div id="news" className="space_section">
      <div className="container">
        <hgroup className="title-main">
          <h3>Tin tức</h3>
        </hgroup>
        <VentureBuilderMainNewsPostsComponent />
      </div>
    </div>
  );
};

export default VentureBuilderMainNewsComponent;
