import { useEffect, useState } from "react";
import { postApi } from "../../../apis";
import { Posts } from "../../../types";

export const usePageData = () => {
  const [posts, setPosts] = useState<Posts[]>();

  const [postLoadingStatus, setPostLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [postError, setPostError] = useState<string>();

  const [aboutPosts, setAboutPosts] = useState<Posts[]>();

  const [aboutPostLoadingStatus, setAboutPostLoadingStatus] = useState<
    "loading" | "done" | "error"
  >("loading");
  const [aboutPostError, setAboutPostError] = useState<string>();

  useEffect(() => {
    postApi
      .findPosts({
        categories: {
          slug: "tin-tuc",
        },
        limit: 8,
      })
      .then((rs) => {
        if (rs.posts) {
          setPostLoadingStatus("done");
          setPosts(rs.posts);
        } else {
          setPostLoadingStatus("error");
          setPostError(rs.error || "");
        }
      })
      .catch((err) => {
        setPostLoadingStatus("error");
        setPostError("unknown error");
      });
  }, []);

  useEffect(() => {
    postApi
      .findPosts({
        categories: {
          slug: "gioi-thieu",
        },
        limit: 4,
      })
      .then((rs) => {
        if (rs.posts) {
          setAboutPostLoadingStatus("done");
          setAboutPosts(rs.posts);
        } else {
          setAboutPostLoadingStatus("error");
          setAboutPostError(rs.error || "");
        }
      })
      .catch((err) => {
        setAboutPostLoadingStatus("error");
        setAboutPostError("unknown error");
      });
  }, []);

  return {
    posts,
    postLoadingStatus,
    postError,
    aboutPosts,
    aboutPostLoadingStatus,
    aboutPostError,
  };
};
