import { createContext } from "react";
import { Posts } from "../../../types";

type ContextType = {
  posts?: Posts[];
  postLoadingStatus: "loading" | "done" | "error";
  postError?: string;
  aboutPosts?: Posts[];
  aboutPostLoadingStatus: "loading" | "done" | "error";
  aboutPostError?: string;
};

export const PageVentureBuilderContext = createContext<ContextType>({
  posts: [],
  postLoadingStatus: "loading",
  aboutPosts: [],
  aboutPostLoadingStatus: "loading",
});
