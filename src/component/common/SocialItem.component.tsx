import React from "react";

const SocialItemComponent = ({
  title,
  linkSocial,
  iconSocial,
}: {
  title: string;
  linkSocial: string;
  iconSocial: string;
}) => {
  return (
    <li>
      <a
        href={linkSocial}
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        title={title}
      >
        <i className={iconSocial} />
      </a>
    </li>
  );
};

export default SocialItemComponent;
