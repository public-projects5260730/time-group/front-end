import React from "react";
import { Link } from "react-router-dom";

const PageLinkItemComponent = ({
  pageName,
  linkPage,
  pathname,
}: {
  pageName: string;
  linkPage: string;
  pathname: string;
}) => {
  return (
    <li className={`nav-item ${linkPage === pathname ? "active" : ""}`}>
      <Link to={linkPage} className="nav-link">
        {pageName}
      </Link>
    </li>
  );
};

export default PageLinkItemComponent;
