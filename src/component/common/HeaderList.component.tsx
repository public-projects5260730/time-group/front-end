import React from "react";
import { useLocation } from "react-router";
import PageLinkItemComponent from "./PageLinkItem.component";

const HeaderListComponent = () => {
  const location = useLocation();
  const pathname = location.pathname;

  return (
    <ul className="navbar-nav header-menu-mobile__list">
      <PageLinkItemComponent
        linkPage="/"
        pageName="Trang chủ"
        pathname={pathname}
      />
      <PageLinkItemComponent
        linkPage="/introduce"
        pageName="Giới thiệu"
        pathname={pathname}
      />
      <PageLinkItemComponent
        linkPage="/venture-builder"
        pageName="Venture Builder"
        pathname={pathname}
      />
      <PageLinkItemComponent
        linkPage="/ecosystem"
        pageName="Hệ sinh thái"
        pathname={pathname}
      />
      <PageLinkItemComponent
        linkPage="/news"
        pageName="Tin tức"
        pathname={pathname}
      />
      <PageLinkItemComponent
        linkPage="/recruit"
        pageName="Tuyển dụng"
        pathname={pathname}
      />
      <PageLinkItemComponent
        linkPage="/contact-us"
        pageName="Liên hệ"
        pathname={pathname}
      />
    </ul>
  );
};

export default HeaderListComponent;
