import React from "react";
import { Link } from "react-router-dom";

const NewsBottomComponentItem = ({
  postImgAlt,
  postTitle,
  postImgLink,
  postLink,
  shortPostParagraph,
}: {
  postImgAlt: string;
  postTitle: string;
  postLink: string;
  postImgLink: string;
  shortPostParagraph: string;
}) => {
  return (
    <div className="post-grid__block-item">
      <div className="content">
        <div>
          <article>
            <figure className="snap">
              <img
                className="img-fluid post-grid-block__item-img"
                src={postImgLink}
                alt={postImgAlt}
              />

              <Link title="Xem chi tiết" to={postLink}>
                <figcaption>
                  <div className="h5">Xem chi tiết</div>
                </figcaption>
              </Link>
            </figure>
            <div className="content">
              <h4>
                <Link to={postLink} title="" className="news__title">
                  {postTitle}
                </Link>
              </h4>
              <p className="news__description">{shortPostParagraph}</p>
            </div>
          </article>
        </div>
      </div>
    </div>
  );
};

export default NewsBottomComponentItem;
