import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { AppState } from "../../types";
import { isDeepEqual } from "../../utils";
import SocialItemComponent from "./SocialItem.component";

const FooterComponent = () => {
  const settings = useSelector(
    (state: AppState) => state.settings.settings,
    isDeepEqual
  );

  return (
    <footer className="space_section">
      <div className="container">
        <div className="row">
          <div className="col-12 col-sm-4">
            <div className="logo-footer">
              <Link to="#" className="logo-footer__link">
                <img
                  src="/assets/images/logo.png"
                  alt="TimeGroup"
                  className="logo-footer__link-img"
                />
              </Link>
              <p>
                TIMEGROUP - Venture Builder công nghệ hàng đầu Việt Nam trong
                các lĩnh vực blockchain, fintech, martech.
              </p>
            </div>
          </div>
          <div className="col-12 col-sm-4 footer__contact">
            <h5 className="footer__contact-title">Thông tin liên hệ</h5>
            <address className="footer__contact-address">
              <img src="/assets/images/v-map.png" alt="Location" />
              {settings?.contact?.address ||
                "V6B – 12A – Văn Phú – Hà Đông – Hà Nội."}
              <br />
              <img src="/assets/images/v-phone.png" alt="Phone" />
              {settings?.contact?.phone || "0589108963"} <br />
              <img src="/assets/images/v-envelope.png" alt="Envelope" />
              {settings?.contact?.email || "infor@timegroup.com"} <br />
              <img src="/assets/images/v-browser.png" alt="Browser" />
              {settings?.contact?.website || "https://timegroup.eco"} <br />
            </address>
          </div>
          <div className="col-12 col-sm-4 footer__social">
            <ul className="fanpages">
              <SocialItemComponent
                iconSocial="fab fa-google"
                linkSocial="#"
                title="Google Plus"
              />
              <SocialItemComponent
                iconSocial="fab fa-youtube"
                linkSocial="#"
                title="Youtube"
              />
              <SocialItemComponent
                iconSocial="fab fa-linkedin"
                linkSocial="#"
                title="Linkedin"
              />
              <SocialItemComponent
                iconSocial="fab fa-telegram"
                linkSocial="#"
                title="Telegram"
              />
              <SocialItemComponent
                iconSocial="fab fa-facebook"
                linkSocial="#"
                title="Facebook"
              />
              <SocialItemComponent
                iconSocial="fab fa-twitter"
                linkSocial="#"
                title="Twitter"
              />
              <SocialItemComponent
                iconSocial="fab fa-instagram"
                linkSocial="#"
                title="Instagram"
              />
            </ul>
            <p className="copyright">
              © 2023 <strong>TimeGroup</strong>, Inc. All rights reserved
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default FooterComponent;
