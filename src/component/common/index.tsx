export { default as PageLinkItemComponent } from "./PageLinkItem.component";
export { default as HeaderComponent } from "./Header.component";
export { default as HeaderListComponent } from "./HeaderList.component";
export { default as SocialItemComponent } from "./SocialItem.component";
export { default as FooterComponent } from "./Footer.component";
export { default as NewsBottomComponentItem } from "./NewsBottomItem.component";
