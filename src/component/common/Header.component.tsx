import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import HeaderListComponent from "./HeaderList.component";

const HeaderComponent = () => {
  const { pathname } = useLocation();
  const [showMenu, setShowMenu] = useState(false);

  useEffect(() => setShowMenu(false), [pathname]);

  return (
    <header id="header">
      <span className="header__screen-darken" />
      <div className="header__menu-logo">
        <div className="header__menu">
          <button
            id="btn-menu"
            data-trigger="header__navbar"
            className="d-lg-none header__menu-button"
            type="button"
            onClick={() => setShowMenu(true)}
          >
            <i className="fa fa-bars header__menu-icon" />
          </button>
        </div>
        <Link className="header__logo-mobile" to="/">
          <img
            src="/assets/images/logo.png"
            alt="Time Group"
            className="header__logo-img"
          />
        </Link>
      </div>

      <aside className="header__menu-mobile-aside">
        <nav
          className={`header__menu-mobile navbar header-menu-mobile__navbar ${
            showMenu ? "showMenu" : ""
          }`}
        >
          <div className="container">
            <div className="header-menu-mobile__logo">
              <Link className="header-menu-mobile__logo-link" to="/">
                <img
                  src="/assets/images/logo.png"
                  alt="Time Group"
                  className="header-menu-mobile__logo-img"
                />
              </Link>
              <button
                className="header-menu-mobile__logo-button"
                onClick={() => setShowMenu(false)}
              >
                &#10005;
              </button>
            </div>
            <HeaderListComponent />
          </div>
        </nav>
        <input
          type="checkbox"
          id="menu-mobile-input"
          onChange={() => setShowMenu(false)}
        />
      </aside>

      <nav id="header__navbar" className="navbar navbar-expand-lg">
        <div className="container">
          <Link className="navbar-brand" to="/">
            <img src="/assets/images/logo.png" alt="Time Group" />
          </Link>
          <HeaderListComponent />
        </div>
      </nav>
    </header>
  );
};

export default HeaderComponent;
