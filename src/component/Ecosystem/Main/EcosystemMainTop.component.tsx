import "./../../../resources/ecosystem/ecosystem-top.scss";

export const EcosystemMainTopComponent = () => {
  return (
    <div className="ecosystem-top ecosystem-bg">
      <div className="container">
        <div>
          <h1>
            <div className="h3">Hệ sinh thái</div>
            <div className="h1">TimeGroup</div>
          </h1>
        </div>
      </div>
    </div>
  );
};
