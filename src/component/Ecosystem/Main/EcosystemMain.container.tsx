import { useEffect, useState } from "react";

import { from } from "rxjs";
import { companyApi } from "../../../apis";
import { ICompanyGroup } from "../../../types";
import { EcosystemMainComponent } from "./EcosystemMain.component";

export const EcosystemMainContainer = () => {
  const [companyGroups, setCompanyGroups] = useState<ICompanyGroup[]>();

  useEffect(() => {
    const subscription = from(companyApi.getGroups()).subscribe(
      ({ companyGroups }) => {
        setCompanyGroups(companyGroups || []);
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  return <EcosystemMainComponent companyGroups={companyGroups} />;
};
