import { useMemo, useState } from "react";
import { ICompany, ICompanyGroup } from "../../../types";
import { OverlayLoading } from "../../tools";
import { Modal } from "react-bootstrap";

import "./../../../resources/ecosystem/ecosystem-main.scss";
import { CompanyViewComponent } from "../View";

export const EcosystemMainComponent = ({
  companyGroups,
}: {
  companyGroups?: ICompanyGroup[];
}) => {
  const [search, setSearch] = useState<string>("");
  const [selectedCompany, setSelectedCompany] = useState<ICompany>();

  const filterdCompanyGroups = useMemo((): ICompanyGroup[] => {
    if (!companyGroups?.length) {
      return [];
    }

    return companyGroups
      .map((group) => ({
        ...group,
        companies: group.companies?.filter(
          ({ name }) =>
            !search?.length || name.toLowerCase().includes(search.toLowerCase())
        ),
      }))
      .filter(({ companies }) => !!companies?.length);
  }, [companyGroups, search]);

  if (companyGroups === undefined) {
    return <OverlayLoading />;
  }

  if (!companyGroups?.length) {
    return <></>;
  }

  return (
    <div className="container">
      <div className="info text-center">
        <h3 className="title text-uppercase">
          Các công ty trong hệ sinh thái timegroup
        </h3>
        {/* <div className="description text-uppercase">
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
          nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat{" "}
        </div> */}
      </div>

      <div className="form">
        <input
          className="form-control"
          placeholder="Search Company Name"
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>

      <div className="company-list">
        {filterdCompanyGroups.map(({ name, companies }, i) => (
          <ul className="list-group" key={i}>
            <li className="list-group-item active text-center text-uppercase">
              {name}
            </li>
            {companies.map((company, j) => (
              <li
                className="list-group-item list-group-item-action cursor-pointer"
                onClick={() => setSelectedCompany(company)}
                key={j}
              >
                {company.name}
              </li>
            ))}
          </ul>
        ))}

        {!!selectedCompany && (
          <Modal
            show={true}
            onHide={() => setSelectedCompany(undefined)}
            centered
            animation={false}
            className="company-view"
          >
            <Modal.Header closeButton />
            <Modal.Body>
              <CompanyViewComponent company={selectedCompany} />
            </Modal.Body>
          </Modal>
        )}
      </div>
    </div>
  );
};
