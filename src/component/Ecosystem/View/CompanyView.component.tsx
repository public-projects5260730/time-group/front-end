import { ICompany } from "../../../types";
import { imageUrl, raw } from "../../../utils";

import "./../../../resources/ecosystem/company-view.scss";

export const CompanyViewComponent = ({ company }: { company: ICompany }) => {
  return (
    <div>
      <div className="info">
        <div className="logo">
          <img
            src={imageUrl(company?.image || company?.logo)}
            alt={company.name}
          ></img>
        </div>
        <div className="detail">
          <div className="title text-uppercase">{company.name}</div>
          <div>
            <img src="/assets/images/v-map.png" alt="address" />
            <span>{company.address}</span>
          </div>

          <div>
            <img src="/assets/images/v-envelope.png" alt="Email" />
            <span>{company.email}</span>
          </div>

          <div>
            <img src="/assets/images/v-phone.png" alt="Phone" />
            <span>{company.phone}</span>
          </div>

          <div>
            <img src="/assets/images/v-website.png" alt="Website" />
            <span>{company.website}</span>
          </div>
        </div>
      </div>

      <div className="company-content">
        <div>
          <div className="title">Brief intro</div>
          <div className="content">{raw(company.brief_intro)}</div>
        </div>
        <div>
          <div className="title">personel</div>
          <div className="content">
            <div className="personel">{company.personel}</div>
            <div className="position">{company.position}</div>
          </div>
        </div>
      </div>
    </div>
  );
};
