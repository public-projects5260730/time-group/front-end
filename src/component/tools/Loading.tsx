export const Loading = ({ minHeight = "80px" }: { minHeight?: string }) => {
  return (
    <div className="overlay dark" style={{ minHeight: minHeight }}>
      <i className="fas fa-circle-o-notch fa-2x fa-spin"></i>
    </div>
  );
};
