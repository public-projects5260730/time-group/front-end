export const CardError = ({
  error,
  title,
}: {
  error: string | React.ReactElement;
  title?: string | React.ReactElement;
}) => {
  return (
    <div className="card card-outline card-danger">
      {title ? (
        <div className="card-header">
          <h3 className="card-title">{title}</h3>
        </div>
      ) : null}
      {error ? (
        <div className="card-body">
          <div className="callout callout-danger text-danger p-2">{error}</div>
        </div>
      ) : null}
    </div>
  );
};
