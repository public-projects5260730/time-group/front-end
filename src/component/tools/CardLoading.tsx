import { Loading } from "./Loading";

export const CardLoading = ({
  minHeight = "80px",
  title,
  content,
}: {
  minHeight?: string;
  title?: string | React.ReactElement;
  content?: string | React.ReactElement;
}) => {
  return (
    <div className="card" style={{ minHeight: minHeight }}>
      {title ? (
        <div className="card-header">
          <h3 className="card-title">{title}</h3>
        </div>
      ) : null}
      {content ? <div className="card-body">{content}</div> : null}
      <Loading minHeight={minHeight} />
    </div>
  );
};
