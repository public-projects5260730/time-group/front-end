export const NoData = ({
  content,
}: {
  content?: string | React.ReactElement;
}) => {
  return (
    <p className="text-center">{content ? content : "No data to display."}</p>
  );
};
