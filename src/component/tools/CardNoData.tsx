import { NoData } from "./NoData";

export const CardNoData = ({
  cardClass = "",
  title,
  content,
}: {
  cardClass?: string;
  title?: string | React.ReactElement;
  content?: string | React.ReactElement;
}) => {
  return (
    <div className={`${cardClass} card`}>
      {title ? (
        <div className="card-header">
          <h3 className="card-title">{title}</h3>
        </div>
      ) : null}
      <div className="card-body">
        <NoData content={content} />
      </div>
    </div>
  );
};
