import { useMemo } from "react";
import ReactPaginate, { ReactPaginateProps } from "react-paginate";
import { useFilters, useWindowWidth } from "../../hooks";
import { IPaginate } from "../../types";

export default function Pagination<T>({
  paginate,
}: {
  paginate?: IPaginate<T>;
}) {
  const { filters, pushFilters } = useFilters();

  const { page, perPage, total } = useMemo(() => {
    if (!paginate?.total) {
      return { page: 1, perPage: 0, total: 0, resultCount: 0 };
    }
    const { page } = { page: 1, ...filters };
    return {
      perPage: paginate.per_page,
      total: paginate.total,
      page: Math.max(1, page),
    };
  }, [paginate, filters]);

  const windowWidth = useWindowWidth();

  const options = useMemo(() => {
    if (!total || total <= perPage) {
      return;
    }
    let options: ReactPaginateProps = {
      forcePage: Math.max(0, page - 1),
      pageCount: Math.ceil(total / perPage),
      pageRangeDisplayed: 6,
      marginPagesDisplayed: 1,
      breakClassName: "page-item",
      breakLinkClassName: "page-link disabled",
      containerClassName: "pagination m-0",
      pageClassName: "page-item",
      pageLinkClassName: "page-link",
      activeClassName: "page-item active",
      activeLinkClassName: "page-link",
      previousClassName: "page-item",
      previousLinkClassName: "page-link",
      nextClassName: "page-item",
      nextLinkClassName: "page-link",
      onPageChange: ({ selected }) =>
        pushFilters({ page: !selected ? "" : selected + 1 }),
    };
    if (windowWidth <= 768) {
      options = {
        ...options,
        ...{
          pageRangeDisplayed: (windowWidth <= 350 && 1) || 3,
          previousLabel: null,
          nextLabel: null,
          previousLinkClassName: "page-link fa fa-angle-left",
          nextLinkClassName: "page-link fa fa-angle-right",
        },
      };
    }
    return options;
  }, [page, perPage, pushFilters, total, windowWidth]);

  if (!options) {
    return <></>;
  }

  return (
    <div className="small d-flex align-items-center justify-content-end flex-wrap-reverse pagination">
      <span className="cursor-pointer ml-2 mt-2 mb-2">
        <ReactPaginate {...options} />
      </span>
    </div>
  );
}
