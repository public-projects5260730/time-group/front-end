import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { recruitApi } from "../../../apis";
import { IRecruit, ISendCVInputs } from "../../../types";
import { raw } from "../../../utils";

export const RecruitSendCVForm = ({ recruit }: { recruit: IRecruit }) => {
  const [isSubmitting, setIsSubmitting] = useState(false);

  const methods = useForm<ISendCVInputs>();

  const {
    handleSubmit,
    register,
    formState: { errors },
    setError,
  } = methods;

  const onSubmit = handleSubmit((formData) => {
    if (isSubmitting) {
      return;
    }
    setIsSubmitting(true);

    recruitApi
      .sendCv(recruit.id, formData)
      .then(({ successfully, errors, error }) => {
        setIsSubmitting(false);
        if (errors) {
          Object.entries(errors).map(([field, message]) => {
            return setError(field as keyof ISendCVInputs, {
              type: "manual",
              message,
            });
          });
        } else if (error?.length) {
          toast.error(error);
        }

        if (successfully) {
          toast.success("Successfully");
        }
      });
  });

  return (
    <div>
      <div className="recruit-body">{raw(recruit.body)}</div>
      <hr />
      <div className="recruit-form">
        <h4>Your profile information</h4>

        <form onSubmit={onSubmit}>
          <div className="inputs">
            <div className="row form-group">
              <label className="col-12 col-form-label" htmlFor="ctrl-email">
                Email Address:
                <span className="ml-1 text-danger" title="Required">
                  (*)
                </span>
              </label>

              <div className="input-group">
                <input
                  type="email"
                  className={`form-control${
                    errors?.email ? " is-invalid" : ""
                  }`}
                  {...register("email", { required: "Email is required." })}
                />
                <div className="invalid-feedback">{errors?.email?.message}</div>
              </div>
              <div className="explain">
                We'll never share your email with anyone else.
              </div>
            </div>

            <div className="row form-group">
              <label className="col-12 col-form-label" htmlFor="ctrl-phone">
                Phone Number:
              </label>

              <div className="input-group">
                <input
                  className={`form-control${
                    errors?.phone ? " is-invalid" : ""
                  }`}
                  {...register("phone")}
                />
                <div className="invalid-feedback">{errors?.phone?.message}</div>
              </div>
              <div className="explain">Your Phone (optional).</div>
            </div>

            <div className="row form-group">
              <label className="col-12 col-form-label" htmlFor="ctrl-fullName">
                Your Name:
                <span className="ml-1 text-danger" title="Required">
                  (*)
                </span>
              </label>

              <div className="input-group">
                <input
                  className={`form-control${
                    errors?.fullName ? " is-invalid" : ""
                  }`}
                  {...register("fullName", {
                    required: "Your Name is required.",
                  })}
                />
                <div className="invalid-feedback">
                  {errors?.fullName?.message}
                </div>
              </div>
            </div>

            <div className="row form-group">
              <label className="col-12 col-form-label" htmlFor="ctrl-fullName">
                Your CV File:
              </label>

              <div className="input-group">
                <input
                  type="file"
                  className={`form-control${
                    errors?.cvFile ? " is-invalid" : ""
                  }`}
                  {...register("cvFile", {
                    validate: {
                      lessThan10MB: (files: any) =>
                        (files[0]?.size || 0) < 5120000 || "Max 5 MB",
                    },
                  })}
                  accept=".doc, .docx, .pdf"
                />
                <div className="invalid-feedback">
                  {errors?.cvFile?.message}
                </div>
              </div>
            </div>
          </div>

          <div className="btn-group pull-right">
            <button type="submit" className="btn btn-primary">
              {isSubmitting && (
                <i className="fa fa-circle-o-notch fa-spin mr-2"></i>
              )}
              Send Infomation
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
