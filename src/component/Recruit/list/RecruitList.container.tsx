import { useEffect, useState } from "react";

import { from } from "rxjs";
import { recruitApi } from "../../../apis";
import { useFilters } from "../../../hooks";
import { IPaginate, IRecruit } from "../../../types";
import { RecruitListComponent } from "./RecruitList.component";

export const RecruitListContainer = () => {
  const { filters } = useFilters();
  const [paginate, setPaginate] = useState<IPaginate<IRecruit> | null>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const subscription = from(
      recruitApi.findPaginate({
        ...filters,
        perPage: 10,
      })
    ).subscribe(({ paginate }) => {
      setPaginate(paginate || null);
      setIsLoading(false);
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [filters]);

  return <RecruitListComponent paginate={paginate} isLoading={isLoading} />;
};
