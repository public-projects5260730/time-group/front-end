import { useState } from "react";
import moment from "moment";
import { IRecruit } from "../../../types";
import { Modal } from "react-bootstrap";
import { RecruitSendCVForm } from "../SendCV";

export const RecruitListTable = ({ recruits }: { recruits: IRecruit[] }) => {
  const [selectRecruit, setSelectRecruit] = useState<IRecruit>();

  return (
    <div className="recruit-table">
      <div className="card">
        <div className="card-body table-responsive">
          <table className="table table-hover">
            <tbody>
              {recruits.map((recruit) => (
                <tr key={recruit.id}>
                  <td>
                    <div className="title">{recruit.title}</div>
                    <div className="description">
                      <div>
                        <span>
                          <img src="/assets/images/clock.png" alt="Work Type" />
                        </span>
                        <span>{recruit.work_type}</span>
                      </div>

                      <div>
                        <span>
                          <img src="/assets/images/v-map.png" alt="Location" />
                        </span>
                        <span>{recruit.location}</span>
                      </div>
                    </div>
                  </td>
                  <td className="pay-rate">{recruit.pay_rate}</td>
                  <td className="expired">
                    {moment.unix(recruit.expired_at).format("DD/MM/YYYY")}
                  </td>
                  <td>
                    <div
                      className="btn btn-outline-primary pull-right"
                      onClick={() => setSelectRecruit(recruit)}
                    >
                      Nộp đơn
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>

      {!!selectRecruit && (
        <Modal
          show={true}
          onHide={() => setSelectRecruit(undefined)}
          centered
          animation={false}
          className="recruit-view"
          size="lg"
        >
          <Modal.Header closeButton>
            <Modal.Title>{selectRecruit.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <RecruitSendCVForm recruit={selectRecruit} />
          </Modal.Body>
        </Modal>
      )}
    </div>
  );
};
