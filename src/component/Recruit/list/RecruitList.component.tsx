import { IPaginate, IRecruit } from "../../../types";
import { formatNumber } from "../../../utils";
import { CardNoData, OverlayLoading } from "../../tools";
import Pagination from "../../tools/Pagination";
import { RecruitListTable } from "./RecruitListTable";

export const RecruitListComponent = ({
  paginate,
  isLoading,
}: {
  paginate?: IPaginate<IRecruit> | null;
  isLoading?: boolean;
}) => {
  if (paginate === undefined) {
    return <OverlayLoading />;
  }
  if (paginate === null || !paginate?.data?.length) {
    return <CardNoData />;
  }

  return (
    <div className="recruit-list love-seat-bg">
      <div className="container">
        <h2 className="text-uppercase text-center">
          CƠ HỘI GIA NHẬP timegroup
        </h2>
        <div className="description">
          {formatNumber(paginate.total)} vị trí đang chờ bạn
        </div>
        {!!isLoading && <OverlayLoading />}
        {<RecruitListTable recruits={paginate.data} />}
        <div className="d-flex justify-content-center mt-5 mb-5">
          <Pagination paginate={paginate} />
        </div>
      </div>
    </div>
  );
};
