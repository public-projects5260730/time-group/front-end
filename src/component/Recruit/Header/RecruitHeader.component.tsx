import { RecruitSearchForm } from "./RecruitSearchForm";

export const RecruitHeaderComponent = () => {
  return (
    <div className="recruit-header recruit-bg">
      <div className="container">
        <RecruitSearchForm />
      </div>
    </div>
  );
};
