import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { from } from "rxjs";
import { recruitApi } from "../../../apis";
import { useFilters } from "../../../hooks";
import { ISelectOptions } from "../../../types";

export const RecruitSearchForm = () => {
  const { filters, pushFilters } = useFilters();
  const [specialismsOptions, setSpecialismsOptions] =
    useState<ISelectOptions[]>();

  const [locationOptions, setLocationOptions] = useState<ISelectOptions[]>();

  useEffect(() => {
    const subscription = from(recruitApi.findOptions()).subscribe(
      ({ specialismsOptions, locationOptions }) => {
        if (specialismsOptions?.length) {
          const options = specialismsOptions.map(
            (value): ISelectOptions => ({
              value,
              label: value,
            })
          );

          options.unshift({
            value: "",
            label: "All Specialisms",
          });

          setSpecialismsOptions(options);
        }
        if (locationOptions?.length) {
          const options = locationOptions.map(
            (value): ISelectOptions => ({
              value,
              label: value,
            })
          );

          options.unshift({
            value: "",
            label: "All Locations",
          });

          setLocationOptions(options);
        }
      }
    );

    return () => {
      subscription.unsubscribe();
    };
  }, []);

  const methods = useForm<any>({
    defaultValues: filters,
  });

  const { handleSubmit, register } = methods;

  const onSubmit = handleSubmit((formData) => {
    pushFilters({ ...formData, page: undefined });
  });

  return (
    <div className="recruit-form">
      <h1>Tuyển dụng</h1>

      <form onSubmit={onSubmit}>
        <div className="input-group">
          <span className="input-group-text">
            <i className="fas fa-search"></i>
          </span>
          <input
            className="form-control"
            placeholder="Search keywords"
            {...register("search")}
          />
          <div className="explain">Search keywords e.g. web design</div>
        </div>
        {!!specialismsOptions?.length && (
          <div className="input-group">
            <select
              className="form-select"
              placeholder="All Specialisms"
              {...register("specialisms")}
            >
              {specialismsOptions?.map(({ value, label }) => (
                <option key={value} value={value}>
                  {label}
                </option>
              ))}
            </select>
            <div className="explain">
              Filter by specialisms e.g. developer, designer
            </div>
          </div>
        )}

        {!!locationOptions?.length && (
          <div className="input-group">
            <select
              className="form-select"
              placeholder="All Locations"
              {...register("location")}
            >
              {locationOptions?.map(({ value, label }) => (
                <option key={value} value={value}>
                  {label}
                </option>
              ))}
            </select>
            <div className="explain">Please select your desired location</div>
          </div>
        )}

        <button type="submit" className="btn btn-primary">
          <i className="fas fa-search mr-2"></i>
          <span>find job</span>
        </button>
      </form>
    </div>
  );
};
