import { config } from "../config";

export const siteName = "Time Group";
export const imgLinktoServer = `${(config?.apiUrl || window.location.origin).replace(/[\/\\\]]*$/, '')}/storage/`;
