export const removeProperties = (obj: any, properties: string[]) => {
  if (obj === null || typeof obj !== 'object' || !properties?.length) {
    return obj;
  }
  const cloneObj = { ...obj };
  properties.forEach((property) => {
    if (typeof cloneObj[property] !== 'undefined') {
      delete cloneObj[property];
    }
  });

  return cloneObj;
};
