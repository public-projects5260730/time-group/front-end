import { IBaseStoreState } from "./../types/Store";
import { hash } from "./hash";

export function hashStore<T>(obj: T): T & IBaseStoreState {
  return { ...obj, __hash: hash(obj), __updateAt: Date.now() };
}
