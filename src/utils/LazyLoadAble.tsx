import React, { lazy, Suspense, ComponentType } from 'react';

export const LazyLoadAble = (
  importFunc: () => Promise<{ default: ComponentType<any> }>
) => {
  const LazyComponent = lazy(importFunc);
  return ({ ...props }) => (
    <Suspense fallback="">
      <LazyComponent {...props} />
    </Suspense>
  );
};
