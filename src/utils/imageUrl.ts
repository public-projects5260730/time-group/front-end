import { config } from "../config";

export const imageUrl = (url: string) => {
  if (!url?.length) {
    return;
  }
  const source = (config?.apiUrl || window.location.origin).replace(
    /[\\/\\\]]*$/,
    ""
  );
  return `${source}/storage/${url}`;
};
