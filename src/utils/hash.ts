export const hash = <T>(
  obj: T,
  options = {
    excludeKeys: (key: string) => ["__hash", "__updateAt"].includes(key),
  }
): string => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const objectHash = require("object-hash");
  const __key = objectHash(window.location.hostname);
  return objectHash({ ...obj, __key }, options);
};
