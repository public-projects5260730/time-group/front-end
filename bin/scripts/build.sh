#!/usr/bin/env bash
mkdir ./build
# sh ./bin/scripts/env-config.sh
sh ./bin/scripts/env-config.dist.sh

# cp ./public/env-config.js ./build/env-config.js
cp ./public/env-config.js.dist ./build/env-config.js.dist

mv .env .env.build

react-scripts build

mv .env.build .env

rm -rf C:/xampp/htdocs/works/time-group/back-end/public/static
cp -r ./build/* C:/xampp/htdocs/works/time-group/back-end/public